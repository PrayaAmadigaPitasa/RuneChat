package net.ardeus.runechat.metrics;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.manager.plugin.MetricsManager;
import net.ardeus.runechat.metrics.service.BStats;

public class MetricsMemory extends MetricsManager {

	private BStats metricsBStats;
	
	private MetricsMemory(RuneChat plugin) {
		super(plugin);
		
		this.metricsBStats = new BStats(plugin);
	}
	
	private static class MetricsMemorySingleton {
		private static final MetricsMemory instance;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			
			instance = new MetricsMemory(plugin);
		}
	}
	
	public static final MetricsMemory getInstance() {
		return MetricsMemorySingleton.instance;
	}
	
	@Override
	public final BStats getMetricsBStats() {
		return this.metricsBStats;
	}
}
