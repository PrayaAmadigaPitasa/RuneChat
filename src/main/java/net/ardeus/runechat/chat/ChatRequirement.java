package net.ardeus.runechat.chat;

import org.bukkit.entity.Player;

import com.praya.agarthalib.support.SupportVault;
import com.praya.agarthalib.utility.SenderUtil;

import api.praya.agarthalib.main.AgarthaLibAPI;
import api.praya.agarthalib.manager.plugin.SupportManagerAPI;

public class ChatRequirement {

	private final String permission;
	private final Double cost;
	
	public ChatRequirement() {
		this(null, null);
	}
	
	public ChatRequirement(String permission, Double cost) {
		this.permission = permission;
		this.cost = cost != null ? Math.max(0, cost) : null;
	}
	
	public final String getPermission() {
		return this.permission;
	}
	
	public final Double getCost() {
		return this.cost;
	}
	
	public final boolean isAllowed(Player player) {
		final SupportManagerAPI supportManagerAPI = AgarthaLibAPI.getInstance().getPluginManagerAPI().getSupportManager();
		final SupportVault supportVault = supportManagerAPI.getSupportVault();
		
		if (player != null) {
			final boolean checkPermission = SenderUtil.hasPermission(player, "runechat.chattype.*") || SenderUtil.hasPermission(player, permission);
			final boolean checkCost;
			
			if (cost != null && supportVault != null) {
				final double balance = supportVault.getBalance(player);
				
				checkCost = balance >= cost;
			} else {
				checkCost = true;
			}
			
			return checkPermission && checkCost;
		} else {
			return false;
		}
	}
	
	public final void onChat(Player player) {
		final SupportManagerAPI supportManagerAPI = AgarthaLibAPI.getInstance().getPluginManagerAPI().getSupportManager();
		final SupportVault supportVault = supportManagerAPI.getSupportVault();
		
		if (player != null) {
			if (cost != null && supportVault != null) {
				supportVault.remBalance(player, cost);
			}
		}
	}
}
