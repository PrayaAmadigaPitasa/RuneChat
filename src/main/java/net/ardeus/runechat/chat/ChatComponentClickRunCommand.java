package net.ardeus.runechat.chat;

public final class ChatComponentClickRunCommand extends ChatComponentClick {

	private static final String CLICK_ACTION = "Run_Command";
	
	public ChatComponentClickRunCommand(String value) {
		super(CLICK_ACTION, value);
	}
	
	@Override
	public final String toString() {
		return "run_command: " + getValue();
	}
}