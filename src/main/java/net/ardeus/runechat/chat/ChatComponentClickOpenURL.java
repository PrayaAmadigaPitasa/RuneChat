package net.ardeus.runechat.chat;

public final class ChatComponentClickOpenURL extends ChatComponentClick {

	private static final String CLICK_ACTION = "Open_URL";
	
	public ChatComponentClickOpenURL(String value) {
		super(CLICK_ACTION, value);
	}
	
	@Override
	public final String toString() {
		return "open_url: " + getValue();
	}
}