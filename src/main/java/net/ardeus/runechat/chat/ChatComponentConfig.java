package net.ardeus.runechat.chat;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import com.praya.agarthalib.utility.FileUtil;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.handler.HandlerConfig;

public final class ChatComponentConfig extends HandlerConfig {

	private static final String PATH_FILE = "Configuration/chat_component.yml";
	
	protected final Map<String, ChatComponent> mapChatComponent = new HashMap<String, ChatComponent>();
	
	protected ChatComponentConfig(RuneChat plugin) {
		super(plugin);
		
		setup();
	}

	@Override
	public final void setup() {
		reset();
		loadAllConfig();
	}

	private final void reset() {
		this.mapChatComponent.clear();
	}
	
	private final void loadAllConfig() {
		final File file = FileUtil.getFile(plugin, PATH_FILE);
		
		if (!file.exists()) {
			FileUtil.saveResource(plugin, PATH_FILE);
		}
		
		final FileConfiguration configFile = FileUtil.getFileConfiguration(file);
		
		loadConfig(configFile);
	}
	
	private final void loadConfig(ConfigurationSection config) {
		if (config != null) {
			for (String key : config.getKeys(false)) {
				final ConfigurationSection chatComponentDataSection = config.getConfigurationSection(key);
				
				String text = null;
				String insertion = null;
				ChatComponentClick click = null;
				ChatComponentHover hover = null;
				
				for (String chatComponentData : chatComponentDataSection.getKeys(false)) {
					if (chatComponentData.equalsIgnoreCase("Text")) {
						text = chatComponentDataSection.getString(chatComponentData);
					} else if (chatComponentData.equalsIgnoreCase("Insertion")) {
						insertion = chatComponentDataSection.getString(chatComponentData);
					} else if (chatComponentData.equalsIgnoreCase("Click")) {
						final ConfigurationSection clickDataSection = chatComponentDataSection.getConfigurationSection(chatComponentData);
						
						String action = null;
						String value = null;
						
						for (String clickData : clickDataSection.getKeys(false)) {
							if (clickData.equalsIgnoreCase("Action")) {
								action = clickDataSection.getString(clickData);
							} else if (clickData.equalsIgnoreCase("Value")) {
								value = clickDataSection.getString(clickData);
							}
						}
						
						if (action != null && value != null) {
							switch (action.toUpperCase()) {
							case "OPEN_URL":
								click = new ChatComponentClickOpenURL(value); break;
							case "RUN_COMMAND":
								click = new ChatComponentClickRunCommand(value); break;
							case "SUGGEST_COMMAND":
								click = new ChatComponentClickSuggestCommand(value); break;
							default:
								continue;
							}
						}
					} else if (chatComponentData.equalsIgnoreCase("Hover")) {
						final List<String> listText = new ArrayList<String>();
						
						if (chatComponentDataSection.isList(chatComponentData)) {
							listText.addAll(chatComponentDataSection.getStringList(chatComponentData));
						} else {
							listText.add(chatComponentDataSection.getString(chatComponentData));
						}
						
						hover = new ChatComponentHoverText(listText);
					}
				}
				
				if (text != null) {
					final ChatComponent chatComponent  = new ChatComponent(key, text, insertion, click, hover);
					
					this.mapChatComponent.put(key, chatComponent);
				}
			}
		}
	}
}
