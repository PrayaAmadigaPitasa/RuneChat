package net.ardeus.runechat.chat;

import org.bukkit.inventory.ItemStack;

public class ChatComponentHoverItem extends ChatComponentHover {

	private static final String HOVER_ACTION = "Show_Item";
	
	private final ItemStack item;
	
	public ChatComponentHoverItem(ItemStack item) {
		super(HOVER_ACTION);
		
		if (item == null) {
			throw new IllegalArgumentException();
		} else {
			this.item = item;
		}
	}
	
	public final ItemStack getItem() {
		return this.item;
	}
	
	@Override
	public final String toString() {
		return "show_item: ";
	}
}
