package net.ardeus.runechat.chat;

import org.bukkit.entity.Entity;

public class ChatComponentHoverEntity extends ChatComponentHover {

	private static final String HOVER_ACTION = "Show_Item";
	
	private final Entity entity;
	
	public ChatComponentHoverEntity(Entity entity) {
		super(HOVER_ACTION);
		
		if (entity == null) {
			throw new IllegalArgumentException();
		} else {
			this.entity = entity;
		}
	}
	
	public final Entity getEntity() {
		return this.entity;
	}
	
	@Override
	public final String toString() {
		return "show_entity: " + "{id:" + getEntity().getUniqueId() + "}";
	}
}
