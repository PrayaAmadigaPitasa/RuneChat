package net.ardeus.runechat.chat;

public abstract class ChatType {

	private final String id;
	private final String name;
	private final String format;
	private final ChatRequirement requirement;
	
	ChatType(String id, String name, String format) {
		this(id, name, format, null);
	}
	
	ChatType(String id, String name, String format, ChatRequirement requirement) {
		if (id == null || format == null) {
			throw new IllegalArgumentException();
		} else {
			this.id = id;
			this.name = name != null ? name : id;
			this.format = format;
			this.requirement = requirement != null ? requirement : new ChatRequirement();
		}
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final String getName() {
		return this.name;
	}
	
	public final String getFormat() {
		return this.format;
	}
	
	public final ChatRequirement getRequirement() {
		return this.requirement;
	}
}
