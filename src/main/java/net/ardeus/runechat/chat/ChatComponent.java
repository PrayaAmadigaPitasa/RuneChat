package net.ardeus.runechat.chat;

public class ChatComponent {

	private final String id;
	private final String text;
	private final String insertion;
	private final ChatComponentClick click;
	private final ChatComponentHover hover;
	
	public ChatComponent(String id, String text, String insertion, ChatComponentClick click, ChatComponentHover hover) {
		if (id == null || text == null) {
			throw new IllegalArgumentException();
		} else {
			this.id = id;
			this.text = text;
			this.insertion = insertion;
			this.click = click;
			this.hover = hover;
		}
	}
	
	public final String getId() {
		return this.id;
	}
	
	public final String getText() {
		return this.text;
	}
	
	public final String getInsertion() {
		return this.insertion;
	}
	
	public final ChatComponentClick getClick() {
		return this.click;
	}
	
	public final ChatComponentHover getHover() {
		return this.hover;
	}
	
	@Override
	public final String toString() {
		final StringBuilder builder = new StringBuilder();
		final String text = getText();
		final String insertion = getInsertion();
		final ChatComponentClick click = getClick();
		final ChatComponentHover hover = getHover();
		
		builder.append("||");
		builder.append(text);
		
		if (insertion !=  null) {
			builder.append("||insertion: " + insertion);
		}
		if (click != null) {
			builder.append("||" + click);
		}
		if (hover != null) {
			builder.append("||" + hover);
		}
		
		builder.append("||");
		
		return builder.toString();
	}
}
