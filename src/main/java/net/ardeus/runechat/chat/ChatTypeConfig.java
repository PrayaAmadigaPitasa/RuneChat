package net.ardeus.runechat.chat;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import com.praya.agarthalib.utility.FileUtil;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.handler.HandlerConfig;

public final class ChatTypeConfig extends HandlerConfig {

	private static final String PATH_FILE = "Configuration/chat_type.yml";
	
	protected final Map<String, ChatType> mapChatType = new HashMap<String, ChatType>();
	
	protected ChatTypeConfig(RuneChat plugin) {
		super(plugin);
		
		setup();
	}

	@Override
	public final void setup() {
		reset();
		loadAllConfig();
	}

	private final void reset() {
		this.mapChatType.clear();
	}
	
	private final void loadAllConfig() {
		final File file = FileUtil.getFile(plugin, PATH_FILE);
		
		if (!file.exists()) {
			FileUtil.saveResource(plugin, PATH_FILE);
		}
		
		final FileConfiguration configFile = FileUtil.getFileConfiguration(file);
		
		loadConfig(configFile);
	}
	
	private final void loadConfig(ConfigurationSection config) {
		if (config != null) {
			for (String key : config.getKeys(false)) {
				final ConfigurationSection chatTypeDataSection = config.getConfigurationSection(key);
				final List<String> blockedWorlds = new ArrayList<String>();
				
				String name = null;
				String type = null;
				String format = null;
				Double radius = null;
				ChatRequirement requirement = null;
				
				for (String chatTypeData : chatTypeDataSection.getKeys(false)) {
					if (chatTypeData.equalsIgnoreCase("Name")) {
						name = chatTypeDataSection.getString(chatTypeData);
					} else if (chatTypeData.equalsIgnoreCase("Type")) {
						type = chatTypeDataSection.getString(chatTypeData);
					} else if (chatTypeData.equalsIgnoreCase("Format")) {
						format = chatTypeDataSection.getString(chatTypeData);
					} else if (chatTypeData.equalsIgnoreCase("Radius")) {
						radius = chatTypeDataSection.getDouble(chatTypeData);
					} else if (chatTypeData.equalsIgnoreCase("Requirement") || chatTypeData.equalsIgnoreCase("Req")) {
						final ConfigurationSection requirementDataSection = chatTypeDataSection.getConfigurationSection(chatTypeData);
						
						String requirementPermission = null;
						Double requirementCost = null;
						
						for (String requirementData : requirementDataSection.getKeys(false)) {
							if (requirementData.equalsIgnoreCase("Permission")) {
								requirementPermission = requirementDataSection.getString(requirementData);
							} else if (requirementData.equalsIgnoreCase("Cost") || requirementData.equalsIgnoreCase("Price")) {
								requirementCost = requirementDataSection.getDouble(requirementData);
							}
						}
						
						requirement = new ChatRequirement(requirementPermission, requirementCost);
					} else if (chatTypeData.equalsIgnoreCase("Blocked_Worlds") || chatTypeData.equalsIgnoreCase("Blocked_World")) {
						if (chatTypeDataSection.isList(chatTypeData)) {
							blockedWorlds.addAll(chatTypeDataSection.getStringList(chatTypeData));
						} else {
							blockedWorlds.add(chatTypeDataSection.getString(chatTypeData));
						}
					}
				}
				
				if (type != null && format != null) {
					final ChatType chatType;
					
					switch (type.toUpperCase()) {
					case "WORLD":
						chatType = new ChatTypeWorld(key, name, format, requirement, radius, blockedWorlds); break;
					case "SERVER":
						chatType =  new ChatTypeServer(key, name, format, requirement); break;
					default:
						continue;
					}
					
					this.mapChatType.put(key, chatType);
				}
			}
		}
	}
}
