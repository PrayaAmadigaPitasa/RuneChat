package net.ardeus.runechat.chat;

import java.util.List;

import org.bukkit.World;

public class ChatTypeWorld extends ChatType {

	private final List<String> blockedWorlds;
	private final Double radius;
	
	public ChatTypeWorld(String id, String name, String format) {
		this(id, name, format, null);
	}
	
	public ChatTypeWorld(String id, String name, String format, ChatRequirement requirement) {
		this(id, name, format, requirement, null);
	}
	
	public ChatTypeWorld(String id, String name, String format, ChatRequirement requirement, Double radius) {
		this(id, name, format, requirement, radius, null);
	}
	
	public ChatTypeWorld(String id, String name, String format, ChatRequirement requirement, Double radius, List<String> blockedWorlds) {
		super(id, name, format, requirement);
		
		this.radius = radius;
		this.blockedWorlds = blockedWorlds != null ? blockedWorlds : null;
	}
	
	public final Double getRadius() {
		return this.radius;
	}
	
	public final List<String> getBlockedWorlds() {
		return this.blockedWorlds;
	}
	
	public final boolean isBlockedWorld(World world) {
		if (world != null) {
			final String worldName = world.getName();
			
			for (String blockedWorld : getBlockedWorlds()) {
				if (blockedWorld.equalsIgnoreCase(worldName)) {
					return true;
				}
			}
		}
		
		return false;
	}
}