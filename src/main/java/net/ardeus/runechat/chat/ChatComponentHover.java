package net.ardeus.runechat.chat;

public abstract class ChatComponentHover {

	private final String action;
	
	ChatComponentHover(String action) {
		if (action == null) {
			throw new IllegalArgumentException();
		} else {
			this.action = action;
		}
	}
	
	public final String getAction() {
		return this.action;
	}
}
