package net.ardeus.runechat.chat;

public abstract class ChatComponentClick {

	private final String action;
	private final String value;
	
	ChatComponentClick(String action, String value) {
		if (action == null || value == null) {
			throw new IllegalArgumentException();
		} else {
			this.action = action;
			this.value = value;
		}
	}
	
	public final String getAction() {
		return this.action;
	}
	
	public final String getValue() {
		return this.value;
	}
}
