package net.ardeus.runechat.chat;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.manager.game.ChatComponentManager;

public final class ChatComponentMemory extends ChatComponentManager {

	private final ChatComponentConfig chatComponentConfig;
	
	private ChatComponentMemory(RuneChat plugin) {
		super(plugin);
		
		this.chatComponentConfig = new ChatComponentConfig(plugin);
	}
	
	private static class ChatComponentMemorySingleton {
		private static final ChatComponentMemory INSTANCE;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			
			INSTANCE = new ChatComponentMemory(plugin);
		}
	}
	
	public static final ChatComponentMemory getInstance() {
		return ChatComponentMemorySingleton.INSTANCE;
	}
	
	protected final ChatComponentConfig getChatComponentConfig() {
		return this.chatComponentConfig;
	}
	
	@Override
	public final List<String> getChatComponentIds() {
		final Map<String, ChatComponent> mapChatComponent = getChatComponentConfig().mapChatComponent;
		
		return new ArrayList<String>(mapChatComponent.keySet());
	}
	
	@Override
	public final List<ChatComponent> getAllChatComponents() {
		final Map<String, ChatComponent> mapChatComponent = getChatComponentConfig().mapChatComponent;
		
		return new ArrayList<ChatComponent>(mapChatComponent.values());
	}
	
	@Override
	public final ChatComponent getChatComponent(String id) {
		if (id != null) {
			final Map<String, ChatComponent> mapChatComponent = getChatComponentConfig().mapChatComponent;
			
			for (String key : mapChatComponent.keySet()) {
				if (key.equalsIgnoreCase(id)) {
					return mapChatComponent.get(key);
				}
			}
		}
		
		return null;
	}
}