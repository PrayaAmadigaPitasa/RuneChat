package net.ardeus.runechat.chat;

import java.util.ArrayList;
import java.util.List;

public class ChatComponentHoverText extends ChatComponentHover {

	private static final String HOVER_ACTION = "Show_Text";
	
	private final List<String> listText;
	
	public ChatComponentHoverText() {
		this(null);
	}
	
	public ChatComponentHoverText(List<String> listText) {
		super(HOVER_ACTION);
		
		this.listText = listText != null ? listText : new ArrayList<String>();
	}
	
	public final List<String> getListText() {
		return this.listText;
	}
	
	@Override
	public final String toString() {
		final StringBuilder builder = new StringBuilder();
		final int size = this.listText.size();
		
		builder.append("show_text: ");
		
		for (int index = 0; index < size; index++) {
			final String text = listText.get(index);
			
			if (index > 0) {
				builder.append("\n");
			}
			
			builder.append(text);
		}
		
		return builder.toString();
	}
}
