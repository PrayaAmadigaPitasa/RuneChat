package net.ardeus.runechat.chat;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.manager.game.ChatTypeManager;

public final class ChatTypeMemory extends ChatTypeManager {

	private final ChatTypeConfig chatTypeConfig;
	
	private ChatTypeMemory(RuneChat plugin) {
		super(plugin);
		
		this.chatTypeConfig = new ChatTypeConfig(plugin);
	}
	
	private static class ChatTypeMemorySingleton {
		private static final ChatTypeMemory INSTANCE;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			
			INSTANCE = new ChatTypeMemory(plugin);
		}
	}
	
	public static final ChatTypeMemory getInstance() {
		return ChatTypeMemorySingleton.INSTANCE;
	}
	
	protected final ChatTypeConfig getChatTypeConfig() {
		return this.chatTypeConfig;
	}

	@Override
	public final List<String> getChatTypeIds() {
		final Map<String, ChatType> mapChatType = getChatTypeConfig().mapChatType;
		
		return new ArrayList<String>(mapChatType.keySet());
	}

	@Override
	public final List<ChatType> getAllChatTypes() {
		final Map<String, ChatType> mapChatType = getChatTypeConfig().mapChatType;
		
		return new ArrayList<ChatType>(mapChatType.values());
	}

	@Override
	public final ChatType getChatType(String id) {		
		if (id != null) {
			final Map<String, ChatType> mapChatType = getChatTypeConfig().mapChatType;
			
			for (String key : mapChatType.keySet()) {
				if (key.equalsIgnoreCase(id)) {
					return mapChatType.get(key);
				}
			}
		}
		
		return null;
	}
}
