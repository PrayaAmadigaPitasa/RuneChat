package net.ardeus.runechat.chat;

public final class ChatComponentClickSuggestCommand extends ChatComponentClick {

	private static final String CLICK_ACTION = "Suggest_Command";
	
	public ChatComponentClickSuggestCommand(String value) {
		super(CLICK_ACTION, value);
	}
	
	@Override
	public final String toString() {
		return "suggest_command: " + getValue();
	}
}