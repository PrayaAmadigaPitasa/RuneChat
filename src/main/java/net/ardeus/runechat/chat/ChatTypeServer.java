package net.ardeus.runechat.chat;

public class ChatTypeServer extends ChatType {
	
	public ChatTypeServer(String id, String name, String format) {
		this(id, name, format, null);
	}
	
	public ChatTypeServer(String id, String name, String format, ChatRequirement requirement) {
		super(id, name, format, requirement);
	}
}