package net.ardeus.runechat;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.manager.player.PlayerChatManager;
import net.ardeus.runechat.manager.player.PlayerManager;
import net.ardeus.runechat.player.PlayerChatMemory;

public final class RuneChatPlayerMemory extends PlayerManager {
	
	private final PlayerChatManager playerChatManager;
	
	protected RuneChatPlayerMemory(RuneChat plugin) {
		super(plugin);
		
		this.playerChatManager = PlayerChatMemory.getInstance();
	}
	
	@Override
	public final PlayerChatManager getPlayerChatManager() {
		return this.playerChatManager;
	}
}