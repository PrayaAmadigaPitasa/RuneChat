package net.ardeus.runechat.groupchat;

import java.util.UUID;

import org.bukkit.OfflinePlayer;

public class GroupChatMember {

	private final UUID groupChatUniqueId;
	private final UUID playerId;
	
	private GroupChatRole role;
	
	protected GroupChatMember(GroupChat groupchat, OfflinePlayer player, GroupChatRole role) {
		this(groupchat.getUniqueId(), player.getUniqueId(), role);
	}
	
	protected GroupChatMember(UUID groupChatUniqueId, UUID playerId, GroupChatRole role) {
		if (groupChatUniqueId == null || playerId == null || role == null) {
			throw new IllegalArgumentException();
		} else {
			this.groupChatUniqueId = groupChatUniqueId;
			this.playerId = playerId;
			this.role = role;
		}
	}
	
	public final GroupChat getGroupChat() {
		final GroupChatMemory groupChatMemory = GroupChatMemory.getInstance();
		final UUID groupChatUniqueId = this.groupChatUniqueId;
		final GroupChat groupChat = groupChatMemory.getGroupChat(groupChatUniqueId);
		
		return groupChat;
	}
	
	public final UUID getPlayerId() {
		return this.playerId;
	}
	
	public final GroupChatRole getRole() {
		return this.role;
	}
	
	public final void setRole(GroupChatRole role) {
		if (role == null) {
			throw new IllegalArgumentException();
		} else {
			final GroupChat groupChat = getGroupChat();
			
			this.role = role;
			
			groupChat.check();
		}
	}
}
