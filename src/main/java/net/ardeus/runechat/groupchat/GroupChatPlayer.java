package net.ardeus.runechat.groupchat;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.bukkit.OfflinePlayer;

public class GroupChatPlayer {

	private final UUID playerId;
	protected final Set<UUID> groupUniqueIds;
	protected final Set<UUID> groupInvitationUniqueIds;

	private UUID activeGroupChatUniqueId;
	private boolean toggle;
	
	public GroupChatPlayer(OfflinePlayer player) {
		this(player, null);
	}
	
	public GroupChatPlayer(OfflinePlayer player, UUID activeGroupChatUniqueId) {
		this(player.getUniqueId(), activeGroupChatUniqueId);
	}
	
	protected GroupChatPlayer(UUID playerId) {
		this(playerId, null);
	}
	
	protected GroupChatPlayer(UUID playerId, UUID activeGroupChatUniqueId) {
		this(playerId, activeGroupChatUniqueId, null);
	}
	
	protected GroupChatPlayer(UUID playerId, UUID activeGroupChatUniqueId, Set<UUID> groupUniqueIds) {
		this(playerId, activeGroupChatUniqueId, groupUniqueIds, null);
	}
	
	protected GroupChatPlayer(UUID playerId, UUID activeGroupChatUniqueId, Set<UUID> groupUniqueIds, Set<UUID> groupInvitationUniqueIds) {
		if (playerId == null) {
			throw new IllegalArgumentException();
		} else {
			this.playerId = playerId;
			this.groupUniqueIds = groupUniqueIds != null ? groupUniqueIds : new HashSet<UUID>();
			this.groupInvitationUniqueIds = groupInvitationUniqueIds != null ? groupInvitationUniqueIds : new HashSet<UUID>();
			this.activeGroupChatUniqueId = activeGroupChatUniqueId;
			this.toggle = true;
		}
	}
	
	public final UUID getPlayerId() {
		return this.playerId;
	}
	
	public final List<GroupChat> getGroupChats() {
		final GroupChatMemory groupChatMemory = GroupChatMemory.getInstance();
		final List<UUID> groupUniqueIds = new ArrayList<UUID>(this.groupUniqueIds);
		final List<GroupChat> groupChats = new ArrayList<GroupChat>();
		
		for (UUID groupUniqueId : groupUniqueIds) {
			final GroupChat groupChat = groupChatMemory.getGroupChat(groupUniqueId);
			
			if (groupChat != null && groupChat.isMember(getPlayerId())) {
				groupChats.add(groupChat);
			} else {
				this.groupUniqueIds.remove(groupUniqueId);
			}
		}
		
		return groupChats;
	}
	
	public final List<UUID> getGroupChatUniqueIds() {
		final List<UUID> groupChatUniqueIds = new ArrayList<UUID>();
		
		for (GroupChat groupChat : getGroupChats()) {
			final UUID groupChatUniqueId = groupChat.getUniqueId();
			
			groupChatUniqueIds.add(groupChatUniqueId);
		}
		
		return groupChatUniqueIds;
	}
	
	public final List<Integer> getGroupChatIds() {
		final List<Integer> groupChatIds = new ArrayList<Integer>();
		
		for (GroupChat groupChat : getGroupChats()) {
			final int groupChatId = groupChat.getId();
			
			groupChatIds.add(groupChatId);
		}
		
		return groupChatIds;
	}
	
	public final GroupChatMember getGroupChatMember(GroupChat groupChat) {
		return groupChat != null ? groupChat.getGroupChatMember(getPlayerId()) : null;
	}
	
	public final boolean isJoinGroupChat(GroupChat groupChat) {
		return getGroupChatMember(groupChat) != null;
	}
	
	public final List<GroupChat> getGroupChatInvitations() {
		final GroupChatMemory groupChatMemory = GroupChatMemory.getInstance();
		final List<UUID> groupInvitationUniqueIds = new ArrayList<UUID>(this.groupInvitationUniqueIds);
		final List<GroupChat> groupChats = new ArrayList<GroupChat>();
		
		for (UUID groupInvitationUniqueId : groupInvitationUniqueIds) {
			final GroupChat groupChat = groupChatMemory.getGroupChat(groupInvitationUniqueId);
			
			if (groupChat != null) {
				groupChats.add(groupChat);
			}
		}
		
		return groupChats;
	}
	
	public final List<UUID> getGroupChatInvitationUniqueIds() {
		final List<UUID> groupChatInvitationUniqueIds = new ArrayList<UUID>();
		
		for (GroupChat groupChat : getGroupChatInvitations()) {
			final UUID groupChatUniqueId = groupChat.getUniqueId();
			
			groupChatInvitationUniqueIds.add(groupChatUniqueId);
		}
		
		return groupChatInvitationUniqueIds;
	}
	
	public final List<Integer> getGroupChatInvitationIds() {
		final List<Integer> groupChatInvitationIds = new ArrayList<Integer>();
		
		for (GroupChat groupChat : getGroupChatInvitations()) {
			final int groupChatId = groupChat.getId();
			
			groupChatInvitationIds.add(groupChatId);
		}
		
		return groupChatInvitationIds;
	}
	
	public final boolean isInvitedByGroupChat(GroupChat groupChat) {
		return groupChat != null ? this.groupInvitationUniqueIds.contains(groupChat.getUniqueId()) : false;
	}
	
	public final boolean addGroupChatInvitation(GroupChat groupChat) {
		if (groupChat != null && !isInvitedByGroupChat(groupChat) && !isJoinGroupChat(groupChat)) {
			final UUID groupChatUniqueId = groupChat.getUniqueId();
			
			this.groupInvitationUniqueIds.add(groupChatUniqueId);
			
			return true;
		} else {
			return false;
		}
	}
	
	public final boolean removeGroupChatInvitation(GroupChat groupChat) {
		if (groupChat != null && isInvitedByGroupChat(groupChat)) {
			final UUID groupChatUniqueId = groupChat.getUniqueId();
			
			this.groupInvitationUniqueIds.remove(groupChatUniqueId);
			
			return true;
		} else {
			return false;
		}
	}
	
	public final GroupChat getActiveGroupChat() {
		final GroupChatMemory groupChatMemory = GroupChatMemory.getInstance();
		final UUID activeUniqueId = this.activeGroupChatUniqueId;
		final GroupChat groupChat = activeUniqueId != null ? groupChatMemory.getGroupChat(activeUniqueId) : null;
		
		if (groupChat != null && groupChat.isMember(getPlayerId())) {
			return groupChat;
		} else {
			if (groupChat != null) {
				final UUID groupChatUniqueId = groupChat.getUniqueId();
				
				this.groupUniqueIds.remove(groupChatUniqueId);
			}
			
			return null;
		}
	}
	
	public final boolean setActiveGroupChat(GroupChat groupChat) {
		final UUID groupChatUniqueId;
		
		if (groupChat != null && groupChat.isMember(getPlayerId())) {
			groupChatUniqueId = groupChat.getUniqueId();
		} else if (groupChat == null) {
			groupChatUniqueId = null;
		} else {
			return false;
		}
		
		this.activeGroupChatUniqueId = groupChatUniqueId;
		
		return true;
	}
	
	public final void removeActiveGroupChat() {
		setActiveGroupChat(null); 
	}
	
	public final boolean getToggle() {
		return this.toggle;
	}
	
	public final void setToggle(boolean flag) {
		this.toggle = flag;
	}
	
	public final boolean leaveGroup() {
		return leaveGroup(getActiveGroupChat());
	}
	
	public final boolean leaveGroup(GroupChat groupChat) {
		if (groupChat != null) {
			return groupChat.kickMember(getPlayerId(), GroupChatQuitReason.LEAVE);
		} else {
			return false;
		}
	}
	
	public final boolean acceptInvitation(GroupChat groupChat) {
		if (groupChat != null && isInvitedByGroupChat(groupChat)) {
			final UUID playerId = getPlayerId();
			final UUID groupChatUniqueId = groupChat.getUniqueId();
			
			if (groupChat.addMember(playerId, GroupChatRole.MEMBER)) {
				
				this.groupInvitationUniqueIds.remove(groupChatUniqueId);
				
				return true;
			}
			
			this.groupInvitationUniqueIds.remove(groupChatUniqueId);
		}
		
		return false;
	}
	
	public final boolean declineInvitation(GroupChat groupChat) {
		if (groupChat != null && isInvitedByGroupChat(groupChat)) {
			final UUID groupChatUniqueId = groupChat.getUniqueId();
			
			this.groupInvitationUniqueIds.remove(groupChatUniqueId);
			
			return true;
		} else {
			return false;
		}
	}
}