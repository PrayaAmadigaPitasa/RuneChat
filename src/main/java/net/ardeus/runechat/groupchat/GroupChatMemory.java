package net.ardeus.runechat.groupchat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.manager.game.GroupChatManager;

public final class GroupChatMemory extends GroupChatManager {

	private final Map<Integer, GroupChat> mapGroupChat = new HashMap<Integer, GroupChat>();
	
	private GroupChatMemory(RuneChat plugin) {
		super(plugin);
	}

	private static class GroupChatMemorySingleton {
		private static final GroupChatMemory INSTANCE;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			
			INSTANCE = new GroupChatMemory(plugin);
		}
	}
	
	public static final GroupChatMemory getInstance() {
		return GroupChatMemorySingleton.INSTANCE;
	}
	
	@Override
	public final List<UUID> getGroupChatUniqueIds() {
		final List<UUID> groupChatUniqueIds = new ArrayList<UUID>();
		
		for (GroupChat groupChat : this.mapGroupChat.values()) {
			final UUID groupChatUniqueId = groupChat.getUniqueId();
			
			groupChatUniqueIds.add(groupChatUniqueId);
		}
		
		return groupChatUniqueIds;
	}
	
	@Override
	public final List<Integer> getGroupChatIds() {
		return new ArrayList<Integer>(this.mapGroupChat.keySet());
	}
	
	@Override
	public final List<GroupChat> getAllGroupChats() {
		return new ArrayList<GroupChat>(this.mapGroupChat.values());
	}
	
	@Override
	public final GroupChat getGroupChat(UUID uniqueId) {
		if (uniqueId != null) {
			for (GroupChat key : this.mapGroupChat.values()) {
				if (key.getUniqueId().equals(uniqueId)) {
					return key;
				}
			}
		}
		
		return null;
	}
	
	@Override
	public final GroupChat getGroupChat(int id) {
		return this.mapGroupChat.get(id);
	}
	
	@Override
	public final GroupChat createGroupChat(String name, OfflinePlayer founder) {
		if (founder != null && name != null) {
			final Integer id = generateGroupChatId();
			
			if (id != null) {
				final GroupChat groupChat = new GroupChat(id, name, founder);
				
				this.mapGroupChat.put(id, groupChat);
				
				return groupChat;
			}
		}
		
		return null;
	}
	
	protected final boolean unregister(GroupChat groupChat) {
		if (groupChat != null && this.mapGroupChat.containsValue(groupChat)) {
			final int id = groupChat.getId();
			
			this.mapGroupChat.remove(id);
			
			return true;
		} else {
			return false;
		}
	}
	
	private final Integer generateGroupChatId() {
		for (int id = 1; id <= 1000000; id++) {
			if (!isExists(id)) {
				return id;
			}
		}
		
		return null;
	}
}