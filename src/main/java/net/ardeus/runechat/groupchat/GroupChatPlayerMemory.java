package net.ardeus.runechat.groupchat;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.manager.game.GroupChatPlayerManager;

public final class GroupChatPlayerMemory extends GroupChatPlayerManager {

	private final Map<UUID, GroupChatPlayer> mapGroupChatPlayer = new HashMap<UUID, GroupChatPlayer>();
	
	private GroupChatPlayerMemory(RuneChat plugin) {
		super(plugin);
	}

	private static class PlayerGroupChatMemorySingleton {
		private static final GroupChatPlayerMemory INSTANCE;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			
			INSTANCE = new GroupChatPlayerMemory(plugin);
		}
	}
	
	public static final GroupChatPlayerMemory getInstance() {
		return PlayerGroupChatMemorySingleton.INSTANCE;
	}
	
	@Override
	public final GroupChatPlayer getGroupChatPlayer(UUID playerId) {
		if (playerId != null) {
			if (!this.mapGroupChatPlayer.containsKey(playerId)) {
				final GroupChatPlayer playerGroupChat = new GroupChatPlayer(playerId);
				
				this.mapGroupChatPlayer.put(playerId, playerGroupChat);
			}
			
			return this.mapGroupChatPlayer.get(playerId);
		} else {
			return null;
		}
	}
}
