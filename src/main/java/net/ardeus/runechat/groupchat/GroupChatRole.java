package net.ardeus.runechat.groupchat;

public enum GroupChatRole {

	ADMIN("Admin"),
	MEMBER("Member");
	
	private final String name;
	
	private GroupChatRole(String name) {
		this.name = name;
	}
	
	public final String getName() {
		return this.name;
	}
	
	public static final GroupChatRole getGroupChatRole(String role) {
		if (role != null) {
			for (GroupChatRole key : GroupChatRole.values()) {
				if (key.toString().equalsIgnoreCase(role)) {
					return key;
				}
			}
		}
		
		return null;
	}
}
