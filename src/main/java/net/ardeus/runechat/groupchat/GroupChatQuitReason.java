package net.ardeus.runechat.groupchat;

public enum GroupChatQuitReason {

	LEAVE,
	KICK,
	DISBAND;
}
