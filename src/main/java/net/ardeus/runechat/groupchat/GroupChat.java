package net.ardeus.runechat.groupchat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.runechat.language.Language;

public class GroupChat {

	private final UUID uuid;
	private final UUID founderId;
	private final int id;
	private final Map<UUID, GroupChatMember> mapGroupChatMember;
	
	private String name;
	
	protected GroupChat(int id, String name, OfflinePlayer founder) {
		this(null, id, name, founder);
	}
	
	protected GroupChat(UUID uuid, int id, String name, OfflinePlayer founder) {
		if (name == null || founder == null) {
			throw new IllegalArgumentException();
		} else {
			final GroupChatPlayerMemory groupChatPlayerMemory = GroupChatPlayerMemory.getInstance();
			final GroupChatPlayer groupChatPlayer = groupChatPlayerMemory.getGroupChatPlayer(founder);
			final UUID groupChatUniqueId = uuid != null ? uuid : UUID.randomUUID();
			final UUID founderId = founder.getUniqueId();
			final GroupChatMember groupChatMember = new GroupChatMember(groupChatUniqueId, founderId, GroupChatRole.ADMIN);
			final Map<UUID, GroupChatMember> mapGroupChatMember = new HashMap<UUID, GroupChatMember>();
			final Set<UUID> groupChatUniqueIds = groupChatPlayer.groupUniqueIds;
			
			groupChatUniqueIds.add(groupChatUniqueId);
			mapGroupChatMember.put(founderId, groupChatMember);
			
			this.uuid = groupChatUniqueId;
			this.founderId = founderId;
			this.id = id;
			this.name = name;
			this.mapGroupChatMember = mapGroupChatMember;
		}
	}
	
	public final UUID getUniqueId() {
		return this.uuid;
	}
	
	public final UUID getFounderId() {
		return this.founderId;
	}
	
	public final boolean isFounder(OfflinePlayer player) {
		return player != null ? player.getUniqueId().equals(getFounderId()) : false;
	}
	
	public final int getId() {
		return this.id;
	}
	
	public final String getName() {
		return this.name;
	}
	
	public final boolean setName(String name) {
		final int limit = 16;
		
		if (name != null && name.length() <= limit) {
			
			this.name = name;
			
			return true;
		} else {
			return false;
		}
	}
	
	public final List<UUID> getMemberIds() {
		return getMemberIds(null);
	}
	
	public final List<UUID> getMemberIds(GroupChatRole role) {
		final List<UUID> memberIds = new ArrayList<UUID>();
		
		for (GroupChatMember groupChatMember : this.mapGroupChatMember.values()) {
			if (role == null || groupChatMember.getRole().equals(role)) {
				final UUID memberId = groupChatMember.getPlayerId();
				
				memberIds.add(memberId);
			}
		}
		
		return memberIds;
	}
	
	public final List<UUID> getActiveMemberIds() {
		return getActiveMemberIds(null);
	}
	
	public final List<UUID> getActiveMemberIds(GroupChatRole role) {
		final GroupChatPlayerMemory groupChatPlayerMemory = GroupChatPlayerMemory.getInstance();
		final List<UUID> activeMemberIds = new ArrayList<UUID>();
		
		for (UUID memberId : getMemberIds(role)) {
			final Player member = PlayerUtil.getOnlinePlayer(memberId);
			
			if (member != null) {
				final GroupChatPlayer groupChatPlayer = groupChatPlayerMemory.getGroupChatPlayer(memberId);
				final GroupChat activeGroupChat = groupChatPlayer.getActiveGroupChat();
				
				if (activeGroupChat == this) {
					activeMemberIds.add(memberId);
				}
				
			}
		}
		
		return activeMemberIds;
	}
	
	public final List<Player> getOnlineActiveMembers() {
		return getOnlineActiveMembers(null);
	}
	
	public final List<Player> getOnlineActiveMembers(GroupChatRole role) {
		final GroupChatPlayerMemory groupChatPlayerMemory = GroupChatPlayerMemory.getInstance();
		final List<Player> onlineActiveMembers = new ArrayList<Player>();
		
		for (UUID memberId : getMemberIds(role)) {
			final Player member = PlayerUtil.getOnlinePlayer(memberId);
			
			if (member != null) {
				final GroupChatPlayer groupChatPlayer = groupChatPlayerMemory.getGroupChatPlayer(memberId);
				final GroupChat activeGroupChat = groupChatPlayer.getActiveGroupChat();
				
				if (activeGroupChat == this) {
					onlineActiveMembers.add(member);
				}
				
			}
		}
		
		return onlineActiveMembers;
	}
	
	public final List<GroupChatMember> getAllMembers() {
		return getAllMembers(null);
	}
	
	public final List<GroupChatMember> getAllMembers(GroupChatRole role) {
		final List<GroupChatMember> allMembers = new ArrayList<GroupChatMember>();
		
		for (GroupChatMember groupChatMember : this.mapGroupChatMember.values()) {
			if (role == null || groupChatMember.getRole().equals(role)) {
				allMembers.add(groupChatMember);
			}
		}
		
		return allMembers;
	}
	
	public final List<Player> getOnlineMembers() {
		return getOnlineMembers(null);
	}
	
	public final List<Player> getOnlineMembers(GroupChatRole role) {
		final List<Player> onlineMembers = new ArrayList<Player>();
		
		for (UUID memberId : getMemberIds(role)) {
			final Player member = PlayerUtil.getOnlinePlayer(memberId);
			
			if (member != null) {
				final GroupChatMember groupChatMember = getGroupChatMember(memberId);
				
				if (role == null || groupChatMember.getRole().equals(role)) {
					onlineMembers.add(member);
				}
			}
		}
		
		return onlineMembers;
	}
	
	public final GroupChatMember getGroupChatMember(OfflinePlayer player) {
		return player != null ? getGroupChatMember(player.getUniqueId()) : null;
	}
	
	public final GroupChatMember getGroupChatMember(UUID playerId) {
		final GroupChatPlayerMemory groupChatPlayerMemory = GroupChatPlayerMemory.getInstance();
		
		if (playerId != null) {
			final GroupChatPlayer groupChatPlayer = groupChatPlayerMemory.getGroupChatPlayer(playerId);
			final Set<UUID> groupUniqueIds = groupChatPlayer.groupUniqueIds;
			final GroupChatMember groupChatMember = this.mapGroupChatMember.get(playerId);
			
			if (groupChatMember == null && groupUniqueIds.contains(playerId)) {
				groupUniqueIds.remove(playerId);
			}
			
			return groupChatMember;
		} else {
			return null;
		}
	}
	
	public final boolean isMember(OfflinePlayer player) {
		return getGroupChatMember(player) != null;
	}
	
	public final boolean isMember(UUID playerId) {
		return getGroupChatMember(playerId) != null;
	}
	
	public final boolean addMember(OfflinePlayer player, GroupChatRole role) {
		if (player != null) {
			final UUID playerId = player.getUniqueId();
			
			return addMember(playerId, role);
		} else {
			return false;
		}
	}
	
	public final boolean addMember(UUID playerId, GroupChatRole role) {
		final GroupChatPlayerMemory groupChatPlayerMemory = GroupChatPlayerMemory.getInstance();
		
		if (playerId != null && role != null && !isMember(playerId)) {
			final UUID groupChatUniqueId = getUniqueId();
			final Player player = PlayerUtil.getOnlinePlayer(playerId);
			final GroupChatPlayer groupChatPlayer = groupChatPlayerMemory.getGroupChatPlayer(player);
			final GroupChatMember groupChatMember = new GroupChatMember(groupChatUniqueId, playerId, role);
			final Set<UUID> groupUniqueIds = groupChatPlayer.groupUniqueIds;
			
			if (player != null) {
				final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
				
				mapPlaceholder.put("player", player.getName());
				mapPlaceholder.put("groupchat_name", getName());
				mapPlaceholder.put("groupchat_id", String.valueOf(getId()));
				
				for (UUID activeMemberId : getActiveMemberIds()) {
					final Player member = PlayerUtil.getOnlinePlayer(activeMemberId);
					
					if (member != null) {
						final MessageBuild message = Language.GROUP_CHAT_JOIN_TO_MEMBER.getMessage(member);
						
						message.sendMessage(member, mapPlaceholder);
					}
				}
				
				final MessageBuild message = Language.GROUP_CHAT_JOIN_TO_PLAYER.getMessage(player);
				
				message.sendMessage(player, mapPlaceholder);
				SenderUtil.playSound(player, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_ON);
			}
			
			this.mapGroupChatMember.put(playerId, groupChatMember);
			
			groupUniqueIds.add(groupChatUniqueId);
			return true;
		} else {
			return false;
		}
	}
	
	public final boolean kickMember(OfflinePlayer player) {
		return kickMember(player, null);
	}
	
	public final boolean kickMember(OfflinePlayer player, GroupChatQuitReason reason) {
		return player != null ? kickMember(player.getUniqueId(), reason) : false;
	}
	
	public final boolean kickMember(UUID playerId) {
		return kickMember(playerId, null);
	}
	
	public final boolean kickMember(UUID playerId, GroupChatQuitReason reason) {
		final GroupChatPlayerMemory groupChatPlayerMemory = GroupChatPlayerMemory.getInstance();
		
		if (playerId != null) {
			final GroupChatPlayer groupChatPlayer = groupChatPlayerMemory.getGroupChatPlayer(playerId);
			final GroupChatMember groupChatMember = getGroupChatMember(playerId);
			final Set<UUID> groupUniqueIds = groupChatPlayer.groupUniqueIds;
		
			groupUniqueIds.remove(getUniqueId());
			
			if (groupChatMember != null) {
				final Player player = PlayerUtil.getOnlinePlayer(playerId);
				final GroupChatRole role = groupChatMember.getRole();
				final GroupChatRole roleAdmin = GroupChatRole.ADMIN;
				final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
				
				mapPlaceholder.put("player", player.getName());
				mapPlaceholder.put("groupchat_name", getName());
				mapPlaceholder.put("groupchat_id", String.valueOf(getId()));
				
				this.mapGroupChatMember.remove(playerId);
				
				if (role.equals(roleAdmin) && getAllMembers(roleAdmin).size() == 0) {
					final MessageBuild message = Language.GROUP_CHAT_QUIT_DISBAND.getMessage(player);
					
					disband();
					message.sendMessage(player, mapPlaceholder);
					SenderUtil.playSound(player, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_ON);
				} else if (reason != null) {
					if (reason.equals(GroupChatQuitReason.LEAVE)) {
						for (UUID memberId : getActiveMemberIds()) {
							final Player member = PlayerUtil.getOnlinePlayer(memberId);
							final MessageBuild message = Language.GROUP_CHAT_QUIT_LEAVE_TO_MEMBER.getMessage(member);
							
							message.sendMessage(member);
						}
						
						final MessageBuild message = Language.GROUP_CHAT_QUIT_LEAVE_TO_PLAYER.getMessage(player);
						
						message.sendMessage(player, mapPlaceholder);
						SenderUtil.playSound(player, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_ON);
					} else if (reason.equals(GroupChatQuitReason.DISBAND)) {
						final MessageBuild message = Language.GROUP_CHAT_QUIT_DISBAND.getMessage(player);
						
						message.sendMessage(player, mapPlaceholder);
						SenderUtil.playSound(player, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_ON);
					}
				}
				
				return true;
			}
		}
		
		return false;
	}
	
	public final boolean disband() {
		final GroupChatMemory groupChatMemory = GroupChatMemory.getInstance();
		
		if (groupChatMemory.unregister(this)) {
			for (UUID memberId : getMemberIds()) {
				kickMember(memberId, GroupChatQuitReason.DISBAND);
			}
			
			return true;
		} else {
			return false;
		}
	}
	
	public final boolean invite(OfflinePlayer player) {
		if (player != null) {
			final UUID playerId = player.getUniqueId();
			
			if (!isMember(playerId)) {
				final GroupChatPlayerMemory groupChatPlayerMemory = GroupChatPlayerMemory.getInstance();
				final GroupChatPlayer groupChatPlayer = groupChatPlayerMemory.getGroupChatPlayer(playerId);
				
				return groupChatPlayer.addGroupChatInvitation(this);
			}
		}
		
		return false;
	}
	
	public final void check() {
		if (getAllMembers(GroupChatRole.ADMIN).size() == 0) {
			disband();
		}
	}
}