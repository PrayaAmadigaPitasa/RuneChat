package net.ardeus.runechat.listener.main;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.bridge.unity.BridgeMessage;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.RuneChatConfig;
import net.ardeus.runechat.chat.ChatComponent;
import net.ardeus.runechat.chat.ChatRequirement;
import net.ardeus.runechat.chat.ChatType;
import net.ardeus.runechat.chat.ChatTypeWorld;
import net.ardeus.runechat.groupchat.GroupChat;
import net.ardeus.runechat.groupchat.GroupChatPlayer;
import net.ardeus.runechat.handler.HandlerListener;
import net.ardeus.runechat.language.Language;
import net.ardeus.runechat.manager.game.ChatComponentManager;
import net.ardeus.runechat.manager.game.GameManager;
import net.ardeus.runechat.manager.game.GroupChatPlayerManager;
import net.ardeus.runechat.manager.player.PlayerChatManager;
import net.ardeus.runechat.manager.player.PlayerManager;
import net.ardeus.runechat.player.PlayerChat;

public class ListenerAsyncPlayerChat extends HandlerListener implements Listener {

	public ListenerAsyncPlayerChat(RuneChat plugin) {
		super(plugin);
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = false)
	public final void onPlayerChat(AsyncPlayerChatEvent event) {
		final RuneChatConfig mainConfig = plugin.getMainConfig();
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final ChatComponentManager chatComponentManager = gameManager.getChatComponentManager();
		final GroupChatPlayerManager groupChatPlayerManager = gameManager.getGroupChatPlayerManager();
		final PlayerChatManager playerChatManager = playerManager.getPlayerChatManager();
		final BridgeMessage bridgeMessage = Bridge.getBridgeMessage();
		final Player player = event.getPlayer();
		final GroupChatPlayer groupChatPlayer = groupChatPlayerManager.getGroupChatPlayer(player);
		final GroupChat groupChat = groupChatPlayer.getActiveGroupChat();
		final boolean groupChatToggle = groupChatPlayer.getToggle();
		
		if (groupChat != null && groupChatToggle) {
			final String message = event.getMessage();
			final String groupChatName = groupChat.getName();
			final UUID groupChatUniqueId = groupChat.getUniqueId();
			final int groupChatId = groupChat.getId();
			final Collection<Player> onlinePlayers = groupChat.getOnlineMembers();
			final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
			
			String format = mainConfig.getChatGroupFormatMessage();
			
			mapPlaceholder.put("message", message);
			mapPlaceholder.put("player_name", player.getName());
			mapPlaceholder.put("player_custom_name", player.getCustomName());
			mapPlaceholder.put("player_uuid", player.getUniqueId().toString());
			mapPlaceholder.put("player_world", player.getWorld().getName());
			mapPlaceholder.put("player_ip", String.valueOf(player.getAddress().getHostName()));
			mapPlaceholder.put("player_level", String.valueOf(player.getLevel()));
			mapPlaceholder.put("groupchat_name", groupChatName);
			mapPlaceholder.put("groupchat_uniqueId", groupChatUniqueId.toString());
			mapPlaceholder.put("groupchat_id", String.valueOf(groupChatId));
			mapPlaceholder.put("groupchat_online_members", String.valueOf(onlinePlayers.size()));
			
			for (ChatComponent chatComponent : chatComponentManager.getFormatChatComponents(format)) {
				final String id = chatComponent.getId();
				final String key = "component:" + id;
				final String serialize = TextUtil.placeholder(mapPlaceholder, chatComponent.toString());
				
				mapPlaceholder.put(key, serialize);
			}
			
			format = TextUtil.placeholder(mapPlaceholder, format);
			format = TextUtil.hookPlaceholderAPI(player, format);
			
			event.setCancelled(true);
			bridgeMessage.sendJson(onlinePlayers, format);
			return;
		} else {
			final PlayerChat playerChat = playerChatManager.getPlayerChat(player);
			final ChatType chatType = playerChat.getChatType();
			
			if (chatType == null) {
				final MessageBuild message = Language.CHAT_TYPE_PLAYER_NOT_SET.getMessage(player);
				
				event.setCancelled(true);
				message.sendMessage(player);
				SenderUtil.playSound(player, SoundEnum.BLOCK_ANVIL_HIT);
				return;
			} else {
				final ChatRequirement chatRequirement = chatType.getRequirement();
				
				if (!chatRequirement.isAllowed(player)) {
					final MessageBuild message = Language.CHAT_TYPE_PLAYER_NOT_ALLOWED.getMessage(player);
					
					event.setCancelled(true);
					message.sendMessage(player);
					SenderUtil.playSound(player, SoundEnum.BLOCK_ANVIL_HIT);
					return;
				} else {
					final World world = player.getWorld();
					
					if (chatType instanceof ChatTypeWorld) {
						final ChatTypeWorld chatTypeWorld = (ChatTypeWorld) chatType;	
						
						if (chatTypeWorld.isBlockedWorld(world)) {
							final MessageBuild message = Language.CHAT_TYPE_WORLD_BLOCKED.getMessage(player);
							
							event.setCancelled(true);
							message.sendMessage(player);
							SenderUtil.playSound(player, SoundEnum.BLOCK_ANVIL_HIT);
							return;
						}
					}
					
					final String message = event.getMessage();
					final Set<Player> blockedPlayers = new HashSet<Player>();
					final Collection<Player> onlinePlayers = PlayerUtil.getOnlinePlayers();
					final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
					
					String format = chatType.getFormat();
					
					mapPlaceholder.put("message", message);
					mapPlaceholder.put("player_name", player.getName());
					mapPlaceholder.put("player_custom_name", player.getCustomName());
					mapPlaceholder.put("player_uuid", player.getUniqueId().toString());
					mapPlaceholder.put("player_world", player.getWorld().getName());
					mapPlaceholder.put("player_ip", String.valueOf(player.getAddress().getHostName()));
					mapPlaceholder.put("player_level", String.valueOf(player.getLevel()));
					
					for (ChatComponent chatComponent : chatComponentManager.getFormatChatComponents(format)) {
						final String id = chatComponent.getId();
						final String key = "component:" + id;
						final String serialize = TextUtil.placeholder(mapPlaceholder, chatComponent.toString());
						
						mapPlaceholder.put(key, serialize);
					}
					
					format = TextUtil.placeholder(mapPlaceholder, format);
					format = TextUtil.hookPlaceholderAPI(player, format);
					
					if (chatType instanceof ChatTypeWorld) {
						final ChatTypeWorld chatTypeWorld = (ChatTypeWorld) chatType;
						final Location location = player.getLocation();
						final Double radius = chatTypeWorld.getRadius();
						
						for (Player onlinePlayer : onlinePlayers) { 
							final World worldPlayer = onlinePlayer.getWorld();
							
							if (worldPlayer.equals(world)) {
								if (radius != null) {
									final Location locationOnlinePlayer = onlinePlayer.getLocation();
									
									if (locationOnlinePlayer.distance(location) > radius) {
										blockedPlayers.add(onlinePlayer);
									}
								}
							} else {
								blockedPlayers.add(onlinePlayer);
							}
						}
					}
					
					event.setCancelled(true);
					onlinePlayers.removeAll(blockedPlayers);
					bridgeMessage.sendJson(onlinePlayers, format);
					return;
				}
			}
		}
	}
}
