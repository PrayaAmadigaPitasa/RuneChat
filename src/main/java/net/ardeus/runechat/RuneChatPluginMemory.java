package net.ardeus.runechat;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.command.CommandMemory;
import net.ardeus.runechat.language.LanguageMemory;
import net.ardeus.runechat.manager.plugin.CommandManager;
import net.ardeus.runechat.manager.plugin.LanguageManager;
import net.ardeus.runechat.manager.plugin.MetricsManager;
import net.ardeus.runechat.manager.plugin.PlaceholderManager;
import net.ardeus.runechat.manager.plugin.PluginManager;
import net.ardeus.runechat.manager.plugin.PluginPropertiesManager;
import net.ardeus.runechat.metrics.MetricsMemory;
import net.ardeus.runechat.placeholder.PlaceholderMemory;
import net.ardeus.runechat.plugin.PluginPropertiesMemory;

public final class RuneChatPluginMemory extends PluginManager {

	private final CommandManager commandManager;
	private final LanguageManager languageManager;
	private final PlaceholderManager placeholderManager;
	private final PluginPropertiesManager pluginPropertiesManager;
	private final MetricsManager metricsManager;
	
	protected RuneChatPluginMemory(RuneChat plugin) {
		super(plugin);
		
		this.commandManager = CommandMemory.getInstance();
		this.placeholderManager = PlaceholderMemory.getInstance();
		this.languageManager = LanguageMemory.getInstance();
		this.pluginPropertiesManager = PluginPropertiesMemory.getInstance();
		this.metricsManager = MetricsMemory.getInstance();
	}
	
	private static class DreamFishPluginMemorySingleton {
		private static final RuneChatPluginMemory instance;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			
			instance = new RuneChatPluginMemory(plugin);
		}
	}
	
	protected static final RuneChatPluginMemory getInstance() {
		return DreamFishPluginMemorySingleton.instance;
	}
	
	public final LanguageManager getLanguageManager() {
		return this.languageManager;
	}
	
	public final PlaceholderManager getPlaceholderManager() {
		return this.placeholderManager;
	}
	
	public final PluginPropertiesManager getPluginPropertiesManager() {
		return this.pluginPropertiesManager;
	}
	
	public final MetricsManager getMetricsManager() {
		return this.metricsManager;
	}
	
	public final CommandManager getCommandManager() {
		return this.commandManager;
	}
}
