package net.ardeus.runechat.player;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.TextUtil;

import core.praya.agarthalib.bridge.unity.Bridge;
import core.praya.agarthalib.bridge.unity.BridgeMessage;
import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.RuneChatConfig;
import net.ardeus.runechat.chat.ChatComponent;
import net.ardeus.runechat.chat.ChatType;
import net.ardeus.runechat.chat.ChatTypeMemory;
import net.ardeus.runechat.manager.game.ChatComponentManager;
import net.ardeus.runechat.manager.game.GameManager;
import net.ardeus.runechat.manager.player.PlayerChatManager;
import net.ardeus.runechat.manager.player.PlayerManager;

public class PlayerChat {

	private final UUID playerId;
	
	private String chatTypeId;
	private UUID lastSender;
	
	public PlayerChat(OfflinePlayer player) {
		this(player.getUniqueId());
	}
	
	public PlayerChat(OfflinePlayer player, ChatType chatType) {
		this(player.getUniqueId(), chatType.getId());
	}
	
	protected PlayerChat(UUID playerId) {
		this(playerId, null);
	}
	
	protected PlayerChat(UUID playerId, String chatTypeId) {
		if (playerId == null) {
			throw new IllegalArgumentException();
		} else {
			this.playerId = playerId;
			this.chatTypeId = chatTypeId;
			this.lastSender = null;
		}
	}
	
	public final UUID getPlayerId() {
		return this.playerId;
	}
	
	public final ChatType getChatType() {
		final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
		final RuneChatConfig mainConfig = plugin.getMainConfig();
		final ChatTypeMemory chatTypeMemory = ChatTypeMemory.getInstance();
		final ChatType chatType = chatTypeMemory.getChatType(this.chatTypeId);
		
		if (chatType != null) {
			return chatType;
		} else {
			final String defaultId = mainConfig.getChatTypeDefaultId();
			final ChatType chatTypeDefault = chatTypeMemory.getChatType(defaultId);
			
			return chatTypeDefault;
		}
	}
	
	public final void setChatType(ChatType chatType) {
		this.chatTypeId = chatType != null ? chatType.getId() : null;
	}
	
	public final UUID getLastSenderId() {
		return this.lastSender;
	}
	
	public final void setLastSender(OfflinePlayer player) {
		if (player != null) {
			final UUID playerId = player.getUniqueId();
			
			setLastSender(playerId);
		}
	}
	
	public final void setLastSender(UUID playerId) {
		this.lastSender = playerId;
	}
	
	public final boolean sendMessage(Player target, String message) {
		final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
		final RuneChatConfig mainConfig = plugin.getMainConfig();
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final ChatComponentManager chatComponentManager = gameManager.getChatComponentManager();
		final PlayerChatManager playerChatManager = playerManager.getPlayerChatManager();
		final BridgeMessage bridgeMessage = Bridge.getBridgeMessage();
		final Player player = PlayerUtil.getOnlinePlayer(getPlayerId());
		
		if (player != null && target != null && message != null) {
			final PlayerChat playerChatTarget = playerChatManager.getPlayerChat(target);
			final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
			
			String formatToSelf = mainConfig.getChatWhisperFormatMessageToSelf();
			String formatToTarget = mainConfig.getChatWhisperFormatMessageToTarget();
			
			mapPlaceholder.put("message", message);
			mapPlaceholder.put("player_name", player.getName());
			mapPlaceholder.put("player_custom_name", player.getCustomName());
			mapPlaceholder.put("player_uuid", player.getUniqueId().toString());
			mapPlaceholder.put("player_world", player.getWorld().getName());
			mapPlaceholder.put("player_ip", String.valueOf(player.getAddress().getHostName()));
			mapPlaceholder.put("player_level", String.valueOf(player.getLevel()));
			mapPlaceholder.put("target_name", target.getName());
			mapPlaceholder.put("target_custom_name", target.getCustomName());
			mapPlaceholder.put("target_uuid", target.getUniqueId().toString());
			mapPlaceholder.put("target_world", target.getWorld().getName());
			mapPlaceholder.put("target_ip", String.valueOf(target.getAddress().getHostName()));
			mapPlaceholder.put("target_level", String.valueOf(target.getLevel()));
			
			for (ChatComponent chatComponent : chatComponentManager.getFormatChatComponents(formatToSelf)) {
				final String id = chatComponent.getId();
				final String key = "component:" + id;
				final String serialize = TextUtil.placeholder(mapPlaceholder, chatComponent.toString());
				
				mapPlaceholder.put(key, serialize);
			}
			
			for (ChatComponent chatComponent : chatComponentManager.getFormatChatComponents(formatToTarget)) {
				final String id = chatComponent.getId();
				final String key = "component:" + id;
				final String serialize = TextUtil.placeholder(mapPlaceholder, chatComponent.toString());
				
				mapPlaceholder.put(key, serialize);
			}
			
			formatToSelf = TextUtil.placeholder(mapPlaceholder, formatToSelf);
			formatToTarget = TextUtil.placeholder(mapPlaceholder, formatToTarget);
			
			formatToSelf = TextUtil.hookPlaceholderAPI(player,formatToSelf);
			formatToTarget = TextUtil.hookPlaceholderAPI(player, formatToTarget);
			
			playerChatTarget.setLastSender(player);
			bridgeMessage.sendJson(player, formatToSelf);
			bridgeMessage.sendJson(target, formatToTarget);
			return true;
		} else {
			return false;
		}
	}
	
	public final boolean replyMessage(String message) {
		final Player target = PlayerUtil.getOnlinePlayer(getLastSenderId());
		
		return sendMessage(target, message);
	}
}
