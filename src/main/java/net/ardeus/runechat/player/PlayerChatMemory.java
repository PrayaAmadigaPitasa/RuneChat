package net.ardeus.runechat.player;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.manager.player.PlayerChatManager;

public final class PlayerChatMemory extends PlayerChatManager {

	private final Map<UUID, PlayerChat> mapPlayerChat = new HashMap<UUID, PlayerChat>();
	
	private PlayerChatMemory(RuneChat plugin) {
		super(plugin);
	}
	
	private static class PlayerChatMemorySingleton {
		private static final PlayerChatMemory INSTANCE;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			
			INSTANCE = new PlayerChatMemory(plugin);
		}
	}

	public static final PlayerChatMemory getInstance() {
		return PlayerChatMemorySingleton.INSTANCE;
	}
	
	@Override
	public final PlayerChat getPlayerChat(UUID playerId) {
		if (playerId != null) {
			if (this.mapPlayerChat.containsKey(playerId)) {
				return this.mapPlayerChat.get(playerId);
			} else {
				final PlayerChat playerChat = new PlayerChat(playerId);
				
				this.mapPlayerChat.put(playerId, playerChat);
				
				return playerChat;
			}
		}
		
		return null;
	}
}
