package net.ardeus.runechat;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.chat.ChatComponentMemory;
import net.ardeus.runechat.chat.ChatTypeMemory;
import net.ardeus.runechat.command.CommandBaseMemory;
import net.ardeus.runechat.groupchat.GroupChatMemory;
import net.ardeus.runechat.groupchat.GroupChatPlayerMemory;
import net.ardeus.runechat.manager.game.ChatComponentManager;
import net.ardeus.runechat.manager.game.ChatTypeManager;
import net.ardeus.runechat.manager.game.CommandBaseManager;
import net.ardeus.runechat.manager.game.GameManager;
import net.ardeus.runechat.manager.game.GroupChatManager;
import net.ardeus.runechat.manager.game.GroupChatPlayerManager;
import net.ardeus.runechat.manager.game.TabCompleterBaseManager;
import net.ardeus.runechat.tabcompleter.TabCompleterTreeMemory;

public final class RuneChatGameMemory extends GameManager {
	
	private final CommandBaseManager commandTreeManager;
	private final TabCompleterBaseManager tabCompleterTreeManager;
	private final ChatComponentManager chatComponentManager;
	private final ChatTypeManager chatTypeManager;
	private final GroupChatManager groupChatManager;
	private final GroupChatPlayerManager groupChatPlayerManager;

	protected RuneChatGameMemory(RuneChat plugin) {
		super(plugin);
		
		this.commandTreeManager = CommandBaseMemory.getInstance();
		this.tabCompleterTreeManager = TabCompleterTreeMemory.getInstance();
		this.chatComponentManager = ChatComponentMemory.getInstance();
		this.chatTypeManager = ChatTypeMemory.getInstance();
		this.groupChatManager = GroupChatMemory.getInstance();
		this.groupChatPlayerManager = GroupChatPlayerMemory.getInstance();
	}
	
	@Override
	public final CommandBaseManager getCommandTreeManager() {
		return this.commandTreeManager;
	}
	
	@Override
	public final TabCompleterBaseManager getTabCompleterTreeManager() {
		return this.tabCompleterTreeManager;
	}
	
	@Override
	public final ChatComponentManager getChatComponentManager() {
		return this.chatComponentManager;
	}
	
	@Override
	public final ChatTypeManager getChatTypeManager() {
		return this.chatTypeManager;
	}
	
	@Override
	public final GroupChatManager getGroupChatManager() {
		return this.groupChatManager;
	}
	
	@Override
	public final GroupChatPlayerManager getGroupChatPlayerManager() {
		return this.groupChatPlayerManager;
	}
}
