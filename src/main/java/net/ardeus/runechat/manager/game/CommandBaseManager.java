package net.ardeus.runechat.manager.game;

import java.util.ArrayList;
import java.util.List;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.command.CommandArgument;
import net.ardeus.runechat.command.CommandBase;
import net.ardeus.runechat.command.CommandBaseFlat;
import net.ardeus.runechat.command.CommandBaseTree;
import net.ardeus.runechat.command.CommandAction;
import net.ardeus.runechat.handler.HandlerManager;

public abstract class CommandBaseManager extends HandlerManager {
	
	protected CommandBaseManager(RuneChat plugin) {
		super(plugin);
	}
	
	public abstract List<String> getCommands();
	public abstract List<CommandBase> getAllCommandBases();
	public abstract CommandBase getCommandBase(String command);
	
	public final boolean isExists(String command) {
		return getCommandBase(command) != null;
	}
	
	public final List<CommandAction> getAllCommandActions() {
		final List<CommandAction> allCommandActions = new ArrayList<CommandAction>();
		
		for (String commandBaseId : getCommands()) {
			final CommandBase commandBase = getCommandBase(commandBaseId);
			
			if (commandBase instanceof CommandBaseFlat) {
				final CommandBaseFlat commandBaseFlat = (CommandBaseFlat) commandBase;
				
				allCommandActions.add(commandBaseFlat);
			} else if (commandBase instanceof CommandBaseTree){
				final CommandBaseTree commandBaseTree = (CommandBaseTree) commandBase;
				
				for (String mainArgument : commandBaseTree.getMainArguments()) {
					final CommandArgument commandArgument = commandBaseTree.getCommandArgument(mainArgument);
					
					allCommandActions.add(commandArgument);
				}
			}
		}
		
		return allCommandActions;
	}
}
