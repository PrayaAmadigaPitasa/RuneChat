package net.ardeus.runechat.manager.game;

import java.util.List;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.chat.ChatType;
import net.ardeus.runechat.handler.HandlerManager;

public abstract class ChatTypeManager extends HandlerManager {

	protected ChatTypeManager(RuneChat plugin) {
		super(plugin);
	}

	public abstract List<String> getChatTypeIds();
	public abstract List<ChatType> getAllChatTypes();
	public abstract ChatType getChatType(String id);
	
	public final boolean isChatTypeExists(String id) {
		return getChatType(id) != null;
	}
}
