package net.ardeus.runechat.manager.game;

import java.util.List;
import java.util.UUID;

import org.bukkit.OfflinePlayer;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.groupchat.GroupChat;
import net.ardeus.runechat.handler.HandlerManager;

public abstract class GroupChatManager extends HandlerManager {

	protected GroupChatManager(RuneChat plugin) {
		super(plugin);
	}

	public abstract List<UUID> getGroupChatUniqueIds();
	public abstract List<Integer> getGroupChatIds();
	public abstract List<GroupChat> getAllGroupChats();
	public abstract GroupChat getGroupChat(UUID uniqueId);
	public abstract GroupChat getGroupChat(int id);
	public abstract GroupChat createGroupChat(String name, OfflinePlayer founder);
	
	public final boolean isExists(UUID uniqueId) {
		return getGroupChat(uniqueId) != null;
	}
	
	public final boolean isExists(int id) {
		return getGroupChat(id) != null;
	}
	
	public final boolean invite(OfflinePlayer player, GroupChat groupChat) {
		return player != null && groupChat != null ? groupChat.invite(player) : false;
	}
}
