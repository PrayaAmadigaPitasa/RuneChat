package net.ardeus.runechat.manager.game;

import java.util.UUID;

import org.bukkit.OfflinePlayer;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.groupchat.GroupChatPlayer;
import net.ardeus.runechat.handler.HandlerManager;

public abstract class GroupChatPlayerManager extends HandlerManager {

	protected GroupChatPlayerManager(RuneChat plugin) {
		super(plugin);
	}

	public abstract GroupChatPlayer getGroupChatPlayer(UUID playerId);
	
	public final GroupChatPlayer getGroupChatPlayer(OfflinePlayer player) {
		return player != null ? getGroupChatPlayer(player.getUniqueId()) : null;
	}
}
