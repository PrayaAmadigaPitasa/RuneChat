package net.ardeus.runechat.manager.game;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.handler.HandlerManager;

public abstract class GameManager extends HandlerManager {

	protected GameManager(RuneChat plugin) {
		super(plugin);
	}
	
	public abstract CommandBaseManager getCommandTreeManager();
	public abstract TabCompleterBaseManager getTabCompleterTreeManager();
	public abstract ChatComponentManager getChatComponentManager();
	public abstract ChatTypeManager getChatTypeManager();
	public abstract GroupChatManager getGroupChatManager();
	public abstract GroupChatPlayerManager getGroupChatPlayerManager();
}
