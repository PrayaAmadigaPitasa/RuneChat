package net.ardeus.runechat.manager.game;

import java.util.List;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.handler.HandlerManager;
import net.ardeus.runechat.tabcompleter.TabCompleterBase;

public abstract class TabCompleterBaseManager extends HandlerManager {
	
	protected TabCompleterBaseManager(RuneChat plugin) {
		super(plugin);
	}
	
	public abstract List<String> getCommands();
	public abstract List<TabCompleterBase> getAllTabCompleterBases();
	public abstract TabCompleterBase getTabCompleterBase(String command);
	
	public final boolean isExists(String command) {
		return getTabCompleterBase(command) != null;
	}
}
