package net.ardeus.runechat.manager.game;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.chat.ChatComponent;
import net.ardeus.runechat.handler.HandlerManager;

public abstract class ChatComponentManager extends HandlerManager {

	protected ChatComponentManager(RuneChat plugin) {
		super(plugin);
	}

	public abstract List<String> getChatComponentIds();
	public abstract List<ChatComponent> getAllChatComponents();
	public abstract ChatComponent getChatComponent(String id);
	
	public final boolean isExists(String id) {
		return getChatComponent(id) != null;
	}
	
	public final List<ChatComponent> getFormatChatComponents(String format) {
		final List<ChatComponent> chatComponents = new ArrayList<ChatComponent>();
		
		if (format != null) {
			final String regex = "\\{component:[^(}|{)]+\\}";
			final Pattern pattern = Pattern.compile(regex);
			final Matcher matcher = pattern.matcher(format);
			
			while (matcher.find()) {
				final String component = matcher.group();
				final int length = component.length();
				final String variable = component.substring(1, (length-1));
				final String[] parts = variable.split("component:");
				
				if (parts.length > 1) {
					final String chatComponentId = parts[1];
					final ChatComponent chatComponent = getChatComponent(chatComponentId);
					
					if (chatComponent != null) {
						chatComponents.add(chatComponent);
					}
				}
			}
		}
		
		return chatComponents;
	}
}
