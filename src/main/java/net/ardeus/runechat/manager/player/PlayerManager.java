package net.ardeus.runechat.manager.player;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.handler.HandlerManager;

public abstract class PlayerManager extends HandlerManager {
	
	protected PlayerManager(RuneChat plugin) {
		super(plugin);
	}
	
	public abstract PlayerChatManager getPlayerChatManager();
}