package net.ardeus.runechat.manager.player;

import java.util.UUID;

import org.bukkit.OfflinePlayer;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.handler.HandlerManager;
import net.ardeus.runechat.player.PlayerChat;

public abstract class PlayerChatManager extends HandlerManager {

	protected PlayerChatManager(RuneChat plugin) {
		super(plugin);
	}
	
	public abstract PlayerChat getPlayerChat(UUID playerId);

	public final PlayerChat getPlayerChat(OfflinePlayer player) {
		return player != null ? getPlayerChat(player.getUniqueId()) : null;
	}
}