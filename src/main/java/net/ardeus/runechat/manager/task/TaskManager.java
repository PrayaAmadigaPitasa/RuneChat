package net.ardeus.runechat.manager.task;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.handler.HandlerManager;

public abstract class TaskManager extends HandlerManager {
	
	protected TaskManager(RuneChat plugin) {
		super(plugin);
	}
}
