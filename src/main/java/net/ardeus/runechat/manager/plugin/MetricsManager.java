package net.ardeus.runechat.manager.plugin;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.handler.HandlerManager;
import net.ardeus.runechat.metrics.service.BStats;

public abstract class MetricsManager extends HandlerManager {
	
	protected MetricsManager(RuneChat plugin) {
		super(plugin);
	}
	
	public abstract BStats getMetricsBStats();
}
