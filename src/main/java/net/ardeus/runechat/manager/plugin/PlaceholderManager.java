package net.ardeus.runechat.manager.plugin;

import java.util.HashMap;
import java.util.List;

import org.bukkit.entity.Player;

import com.praya.agarthalib.utility.ListUtil;
import com.praya.agarthalib.utility.PluginUtil;
import com.praya.agarthalib.utility.TextUtil;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.handler.HandlerManager;
import net.ardeus.runechat.placeholder.replacer.ReplacerMVDWPlaceholderAPI;
import net.ardeus.runechat.placeholder.replacer.ReplacerPlaceholderAPI;

public abstract class PlaceholderManager extends HandlerManager {
	
	protected PlaceholderManager(RuneChat plugin) {
		super(plugin);
	};
	
	public abstract List<String> getPlaceholderIds();
	public abstract List<String> getPlaceholders();
	public abstract String getPlaceholder(String id);
	public abstract String localPlaceholder(String text);
	
	public final boolean isPlaceholderExists(String id) {
		return getPlaceholder(id) != null;
	}
	
	public final void registerAll() {
		final String placeholder = plugin.getPluginPlaceholder();
		
		if (PluginUtil.isPluginInstalled("PlaceholderAPI")) {
			new ReplacerPlaceholderAPI(plugin, placeholder).hook();
		}
		
		if (PluginUtil.isPluginInstalled("MVdWPlaceholderAPI")) {
			new ReplacerMVDWPlaceholderAPI(plugin, placeholder).register();
		}
	}
	
	public final List<String> localPlaceholder(List<String> list) {
		final String divider = "\n";
		final String builder = TextUtil.convertListToString(list, divider);
		final String text = localPlaceholder(builder);
		
		return ListUtil.convertStringToList(text, divider);
	}
	
	public final List<String> pluginPlaceholder(List<String> list, String... identifiers) {
		return pluginPlaceholder(list, null, identifiers);
	}
	
	public final List<String> pluginPlaceholder(List<String> list, Player player, String... identifiers) {
		final String divider = "\n";
		final String builder = TextUtil.convertListToString(list, divider);
		final String text = pluginPlaceholder(builder, player, identifiers);
		
		return ListUtil.convertStringToList(text, divider);
	}
	
	public final String pluginPlaceholder(String text, String... identifiers) {
		return pluginPlaceholder(text, null, identifiers);
	}
	
	public final String pluginPlaceholder(String text, Player player, String... identifiers) {
		final HashMap<String, String> map = getMapPluginPlaceholder(player, identifiers);
		
		return TextUtil.placeholder(map, text);
	}
	
	public final HashMap<String, String> getMapPluginPlaceholder(String... identifiers) {
		return getMapPluginPlaceholder(null, identifiers);
	}
	
	public final HashMap<String, String> getMapPluginPlaceholder(Player player, String... identifiers) {
		final String placeholder = plugin.getPluginPlaceholder();
		final HashMap<String, String> map = new HashMap<String, String>();
		
		for (String identifier : identifiers) {
			final String replacement = getReplacement(player, identifier);
			
			if (replacement != null) {
				final String key = placeholder + "_" + identifier;
				
				map.put(key, replacement);
			}
		}
		
		return map;
	}
	
	public final String getReplacement(Player player, String identifier) {
		return null;
	}
}