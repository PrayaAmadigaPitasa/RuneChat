package net.ardeus.runechat.manager.plugin;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.handler.HandlerManager;

public abstract class PluginManager extends HandlerManager {
	
	protected PluginManager(RuneChat plugin) {
		super(plugin);
	}
	
	public abstract LanguageManager getLanguageManager();
	public abstract PlaceholderManager getPlaceholderManager();
	public abstract PluginPropertiesManager getPluginPropertiesManager();
	public abstract MetricsManager getMetricsManager();
	public abstract CommandManager getCommandManager();
}
