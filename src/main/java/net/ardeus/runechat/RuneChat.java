package net.ardeus.runechat;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitScheduler;

import core.praya.agarthalib.builder.face.Agartha;
import net.ardeus.runechat.listener.main.ListenerAsyncPlayerChat;
import net.ardeus.runechat.manager.game.GameManager;
import net.ardeus.runechat.manager.player.PlayerManager;
import net.ardeus.runechat.manager.plugin.PluginManager;
import net.ardeus.runechat.manager.task.TaskManager;

import com.praya.agarthalib.utility.ServerEventUtil;

public class RuneChat extends JavaPlugin implements Agartha {
	
	private final String type = "Premium";
	private final String placeholder = "runechat";
	
	private RuneChatConfig mainConfig;
	
	private PluginManager pluginManager;
	private PlayerManager playerManager;
	private GameManager gameManager;
	private TaskManager taskManager;
	
	public static final RuneChat getInstance() {
		return JavaPlugin.getPlugin(RuneChat.class);
	}
	
	@Override
	public String getPluginName() {
		return this.getName();
	}

	@Override
	public String getPluginType() {
		return this.type;
	}
	
	@Override
	public String getPluginVersion() {
		return getDescription().getVersion();
	}
	
	@Override
	public String getPluginPlaceholder() {
		return this.placeholder;
	}

	@Override
	public String getPluginWebsite() {
		return getPluginManager().getPluginPropertiesManager().getPluginWebsite();
	}

	@Override
	public String getPluginLatest() {
		return getPluginManager().getPluginPropertiesManager().getPluginVersion();
	}
	
	@Override
	public List<String> getPluginDevelopers() {
		return getPluginManager().getPluginPropertiesManager().getPluginDevelopers();
	}
	
	public final RuneChatConfig getMainConfig() {
		return this.mainConfig;
	}
	
	public final PluginManager getPluginManager() {
		return this.pluginManager;
	}
	
	public final PlayerManager getPlayerManager() {
		return this.playerManager;
	}
	
	public final GameManager getGameManager() {
		return this.gameManager;
	}
	
	public final TaskManager getTaskManager() {
		return this.taskManager;
	}
	
	@Override
	public void onEnable() {
		registerConfig();
		registerManager();
		registerListener();
		registerPlaceholder();
	}
	
	@Override
	public final void onDisable() {
		final BukkitScheduler scheduler = Bukkit.getScheduler();
		
		scheduler.cancelTasks(this);
	}
	
	private final void registerConfig() {
		this.mainConfig = new RuneChatConfig(this);
	}
	
	private final void registerManager() {
		this.pluginManager = new RuneChatPluginMemory(this);
		this.playerManager = new RuneChatPlayerMemory(this);
		this.gameManager = new RuneChatGameMemory(this);
		this.taskManager = new RuneChatTaskMemory(this);
	}
	
	private final void registerPlaceholder() {
		getPluginManager().getPlaceholderManager().registerAll();
	}
	
	private final void registerListener() {
		final Listener listenerAsyncPlayerChat = new ListenerAsyncPlayerChat(this);
		
		ServerEventUtil.registerEvent(this, listenerAsyncPlayerChat);
	}
}
