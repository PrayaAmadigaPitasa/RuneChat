package net.ardeus.runechat.language;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.builder.main.LanguageBuild;
import core.praya.agarthalib.builder.message.MessageBuild;
import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.language.LanguageConfig;
import net.ardeus.runechat.manager.plugin.LanguageManager;

public final class LanguageMemory extends LanguageManager {

	private final LanguageConfig languageConfig;
	
	private LanguageMemory(RuneChat plugin) {
		super(plugin);
		
		this.languageConfig = new LanguageConfig(plugin);
	};
	
	private static class LanguageMemorySingleton {
		private static final LanguageMemory instance;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			
			instance = new LanguageMemory(plugin);
		}
	}
	
	public static final LanguageMemory getInstance() {
		return LanguageMemorySingleton.instance;
	}
	
	public final LanguageConfig getLanguageConfig() {
		return this.languageConfig;
	}
	
	@Override
	public final List<String> getLanguageIds() {
		final Map<String, LanguageBuild> mapLanguageBuild = getLanguageConfig().mapLanguageBuild;
		
		return new ArrayList<String>(mapLanguageBuild.keySet());
	}
	
	@Override
	public final List<LanguageBuild> getAllLanguageBuild() {
		final Map<String, LanguageBuild> mapLanguageBuild = getLanguageConfig().mapLanguageBuild;
		
		return new ArrayList<LanguageBuild>(mapLanguageBuild.values());
	}
	
	protected final Collection<LanguageBuild> getAllLanguageBuild(boolean clone) {
		final Collection<LanguageBuild> allLanguageBuild = getLanguageConfig().mapLanguageBuild.values();
		
		return clone ? new ArrayList<LanguageBuild>(allLanguageBuild) : allLanguageBuild; 
	}
	
	@Override
	public final LanguageBuild getLocaleLanguage(String id) {
		if (id != null) {
			final Map<String, LanguageBuild> mapLanguageBuild = getLanguageConfig().mapLanguageBuild;
			
			for (String key : mapLanguageBuild.keySet()) {
				if (key.equalsIgnoreCase(id)) {
					return getLanguageConfig().mapLanguageBuild.get(key);
				}
			}
		}
		
		return null;
	}
	
	@Override
	public final MessageBuild getLocaleMessage(String id, String key) {
		if (id != null && key != null) {
			final LanguageBuild languageBuild = getLocaleLanguage(id);
			
			if (languageBuild != null) {
				return languageBuild.getMessage(key);
			}
		}
		
		return new MessageBuild();
	}
}