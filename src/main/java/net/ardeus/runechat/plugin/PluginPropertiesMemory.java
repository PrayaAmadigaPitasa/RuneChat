package net.ardeus.runechat.plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.builder.plugin.PluginPropertiesBuild;
import core.praya.agarthalib.builder.plugin.PluginPropertiesResourceBuild;
import core.praya.agarthalib.builder.plugin.PluginPropertiesStreamBuild;
import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.manager.plugin.PluginPropertiesManager;

public final class PluginPropertiesMemory extends PluginPropertiesManager {
	
	private final PluginPropertiesConfig pluginPropertiesConfig;
	private final PluginPropertiesResourceBuild pluginPropertiesResource;
	private final PluginPropertiesStreamBuild pluginPropertiesStream;
	
	private PluginPropertiesMemory(RuneChat plugin) {
		super(plugin);
		
		final String name = plugin.getName();
		final PluginPropertiesConfig pluginPropertiesConfig = new PluginPropertiesConfig(plugin);
		final PluginPropertiesResourceBuild pluginPropertiesResource = PluginPropertiesBuild.getPluginPropertiesResource(plugin, plugin.getPluginType(), plugin.getPluginVersion());
		final PluginPropertiesStreamBuild pluginPropertiesStream = pluginPropertiesConfig.mapPluginProperties.get(name);
		
		this.pluginPropertiesConfig = pluginPropertiesConfig;
		this.pluginPropertiesResource = pluginPropertiesResource;
		this.pluginPropertiesStream = pluginPropertiesStream != null ? pluginPropertiesStream : new PluginPropertiesStreamBuild();
	}
	
	private static class PluginPropertiesMemorySingleton {
		private static final PluginPropertiesMemory instance;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			
			instance = new PluginPropertiesMemory(plugin);
		}
	}
	
	public static final PluginPropertiesMemory getInstance() {
		return PluginPropertiesMemorySingleton.instance;
	}
	
	public final PluginPropertiesConfig getPluginPropertiesConfig() {
		return this.pluginPropertiesConfig;
	}
	
	@Override
	public final PluginPropertiesResourceBuild getPluginPropertiesResource() {
		return this.pluginPropertiesResource;
	}
	
	@Override
	public final PluginPropertiesStreamBuild getPluginPropertiesStream() {
		return this.pluginPropertiesStream;
	}
	
	@Override
	public final List<String> getPluginIds() {
		final Map<String, PluginPropertiesStreamBuild> mapPluginProperties = getPluginPropertiesConfig().mapPluginProperties;
		
		return new ArrayList<String>(mapPluginProperties.keySet());
	}
	
	@Override
	public final List<PluginPropertiesStreamBuild> getAllPluginProperties() {
		final Map<String, PluginPropertiesStreamBuild> mapPluginProperties = getPluginPropertiesConfig().mapPluginProperties;
		
		return new ArrayList<PluginPropertiesStreamBuild>(mapPluginProperties.values());
	}
	
	@Override
	public final PluginPropertiesStreamBuild getPluginProperties(String id) {
		if (id != null) {
			for (String key : getPluginIds()) {
				if (key.equalsIgnoreCase(id)) {
					return getPluginPropertiesConfig().mapPluginProperties.get(key);
				}
			}
		}
		
		return null;
	}
}
