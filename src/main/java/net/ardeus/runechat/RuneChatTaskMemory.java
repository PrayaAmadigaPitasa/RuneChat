package net.ardeus.runechat;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.manager.task.TaskManager;

public final class RuneChatTaskMemory extends TaskManager {
	
	protected RuneChatTaskMemory(RuneChat plugin) {
		super(plugin);
	}
}