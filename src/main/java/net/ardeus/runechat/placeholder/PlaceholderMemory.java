package net.ardeus.runechat.placeholder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.TextUtil;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.manager.plugin.PlaceholderManager;

public final class PlaceholderMemory extends PlaceholderManager {
	
	private final PlaceholderConfig placeholderConfig;
	
	private PlaceholderMemory(RuneChat plugin) {
		super(plugin);
		
		this.placeholderConfig = new PlaceholderConfig(plugin);
	};
	
	private static class PlaceholderMemorySingleton {
		private static final PlaceholderMemory instance;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			
			instance = new PlaceholderMemory(plugin);
		}
	}
	
	public static final PlaceholderMemory getInstance() {
		return PlaceholderMemorySingleton.instance;
	}
	
	public final PlaceholderConfig getPlaceholderConfig() {
		return this.placeholderConfig;
	}
	
	@Override
	public final List<String> getPlaceholderIds() {
		final Map<String, String> mapPlaceholder = getPlaceholderConfig().mapPlaceholder;
		
		return new ArrayList<String>(mapPlaceholder.keySet());
	}
	
	@Override
	public final List<String> getPlaceholders() {
		final Map<String, String> mapPlaceholder = getPlaceholderConfig().mapPlaceholder;
		
		return new ArrayList<String>(mapPlaceholder.values());
	}
	
	@Override
	public final String getPlaceholder(String id) {
		for (String key : getPlaceholderIds()) {
			if (key.equalsIgnoreCase(id)) {
				return getPlaceholderConfig().mapPlaceholder.get(id);
			}
		}
		
		return null;
	}
	
	@Override
	public final String localPlaceholder(String text) {
		final HashMap<String, String> mapPlaceholder = getPlaceholderConfig().mapPlaceholder;
		
		return TextUtil.placeholder(mapPlaceholder, text);
	}
}
	