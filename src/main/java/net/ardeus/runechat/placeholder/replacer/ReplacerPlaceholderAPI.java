package net.ardeus.runechat.placeholder.replacer;

import org.bukkit.entity.Player;

import me.clip.placeholderapi.PlaceholderAPI;
import me.clip.placeholderapi.PlaceholderHook;
import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.manager.plugin.PlaceholderManager;

public class ReplacerPlaceholderAPI extends PlaceholderHook {
	
	private final String placeholder;
	private final RuneChat plugin;
	
	public ReplacerPlaceholderAPI(RuneChat plugin, String placeholder) {
		this.plugin = plugin;
		this.placeholder = placeholder;
	}
	
	public final String getPlaceholder() {
		return this.placeholder;
	}
	
	public final boolean hook() {
		return PlaceholderAPI.registerPlaceholderHook(this.placeholder, this);
	}

	@Override
	public String onPlaceholderRequest(Player player, String identifier) {
		final PlaceholderManager placeholderManager = plugin.getPluginManager().getPlaceholderManager();
		
		return placeholderManager.getReplacement(player, identifier);
	}
}