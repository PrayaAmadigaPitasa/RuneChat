package net.ardeus.runechat.placeholder.replacer;

import be.maximvdw.placeholderapi.PlaceholderAPI;
import be.maximvdw.placeholderapi.PlaceholderReplaceEvent;
import be.maximvdw.placeholderapi.PlaceholderReplacer;
import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.manager.plugin.PlaceholderManager;

public class ReplacerMVDWPlaceholderAPI {

	private final RuneChat plugin;
	private final String placeholder;
	
	public ReplacerMVDWPlaceholderAPI(RuneChat plugin, String placeholder) {
		this.plugin = plugin;
		this.placeholder = placeholder;
	}
	
	public final String getPlaceholder() {
		return this.placeholder;
	}
	
	public final void register() {
		final PlaceholderManager placeholderManager = plugin.getPluginManager().getPlaceholderManager();
		final String identifier = getPlaceholder() + "_*";
		
		PlaceholderAPI.registerPlaceholder(plugin, identifier, new PlaceholderReplacer() {
			
			@Override
			public String onPlaceholderReplace(PlaceholderReplaceEvent event) {
				return placeholderManager.getReplacement(event.getPlayer(), event.getPlaceholder().split(getPlaceholder() + "_")[1]);
			}
		});
	}
}
