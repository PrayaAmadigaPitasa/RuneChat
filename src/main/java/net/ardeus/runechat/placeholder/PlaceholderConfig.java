package net.ardeus.runechat.placeholder;

import java.io.File;
import java.util.HashMap;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import com.praya.agarthalib.utility.FileUtil;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.handler.HandlerConfig;

public final class PlaceholderConfig extends HandlerConfig {
	
	private static final String PATH_FILE = "Configuration/placeholder.yml";
	
	protected final HashMap<String, String> mapPlaceholder = new HashMap<String, String>(); 
	
	protected PlaceholderConfig(RuneChat plugin) {
		super(plugin);
		
		setup();
	};
	
	public final void setup() {
		reset();
		loadAllConfig();
	}
	
	private final void reset() {
		this.mapPlaceholder.clear();
	}
	
	private final void loadAllConfig() {
		final File file = FileUtil.getFile(plugin, PATH_FILE);
		
		if (!file.exists()) {
			FileUtil.saveResource(plugin, PATH_FILE);
		}
		
		final FileConfiguration configResource = FileUtil.getFileConfigurationResource(plugin, PATH_FILE);
		final FileConfiguration configFile = FileUtil.getFileConfiguration(file);
		
		loadConfig(configResource);
		loadConfig(configFile);
	}
	
	public final void loadConfig(ConfigurationSection config) {
		if (config != null) {
			for (String key : config.getKeys(false)) {
				if (key.equalsIgnoreCase("Placeholder")) {
					final ConfigurationSection placeholderSection = config.getConfigurationSection(key);
					
					for (String placeholder : placeholderSection.getKeys(false)) {
						this.mapPlaceholder.put(placeholder, placeholderSection.getString(placeholder));
					}
				}
			}
		}
	}
}
