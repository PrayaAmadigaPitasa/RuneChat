package net.ardeus.runechat.handler;

import java.util.ArrayList;
import java.util.Collection;

import net.ardeus.runechat.RuneChat;

public abstract class HandlerConfig extends Handler {
	
	protected HandlerConfig(RuneChat plugin) {
		super(plugin);
	}
	
	public abstract void setup();
	
	public static Collection<HandlerConfig> getAllHandlerConfig() {
		final Collection<Handler> allHandler = getAllHandler();
		final Collection<HandlerConfig> allHandlerConfig = new ArrayList<HandlerConfig>();
		
		for (Handler handler : allHandler) {
			if (handler instanceof HandlerConfig) {
				final HandlerConfig handlerConfig = (HandlerConfig) handler;
				
				allHandlerConfig.add(handlerConfig);
			}
		}
		
		return allHandlerConfig;
	}
}