package net.ardeus.runechat.handler;

import java.util.ArrayList;
import java.util.Collection;

import net.ardeus.runechat.RuneChat;

public abstract class HandlerManager extends Handler {
	
	protected HandlerManager(RuneChat plugin) {
		super(plugin);
	}
	
	public static Collection<HandlerManager> getAllHandlerManager() {
		final Collection<Handler> allHandler = getAllHandler();
		final Collection<HandlerManager> allHandlerManager = new ArrayList<HandlerManager>();
		
		for (Handler handler : allHandler) {
			if (handler instanceof HandlerManager) {
				final HandlerManager handlerManager = (HandlerManager) handler;
				
				allHandlerManager.add(handlerManager);
			}
		}
		
		return allHandlerManager;
	}
}
