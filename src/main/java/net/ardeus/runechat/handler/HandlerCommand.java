package net.ardeus.runechat.handler;

import java.util.ArrayList;
import java.util.Collection;

import net.ardeus.runechat.RuneChat;

public abstract class HandlerCommand extends Handler {
	
	protected HandlerCommand(RuneChat plugin) {
		super(plugin);
	}
	
	public static Collection<HandlerCommand> getAllHandlerCommand() {
		final Collection<Handler> allHandler = getAllHandler();
		final Collection<HandlerCommand> allHandlerCommand = new ArrayList<HandlerCommand>();
		
		for (Handler handler : allHandler) {
			if (handler instanceof HandlerCommand) {
				final HandlerCommand handlerCommand = (HandlerCommand) handler;
				
				allHandlerCommand.add(handlerCommand);
			}
		}
		
		return allHandlerCommand;
	}
}
