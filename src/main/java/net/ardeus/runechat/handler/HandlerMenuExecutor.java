package net.ardeus.runechat.handler;

import java.util.ArrayList;
import java.util.Collection;

import net.ardeus.runechat.RuneChat;

public abstract class HandlerMenuExecutor extends Handler {
	
	protected HandlerMenuExecutor(RuneChat plugin) {
		super(plugin);
	}
	
	public static Collection<HandlerMenuExecutor> getAllHandlerMenuExecutor() {
		final Collection<Handler> allHandler = getAllHandler();
		final Collection<HandlerMenuExecutor> allHandlerMenuExecutor = new ArrayList<HandlerMenuExecutor>();
		
		for (Handler handler : allHandler) {
			if (handler instanceof HandlerMenuExecutor) {
				final HandlerMenuExecutor handlerMenu = (HandlerMenuExecutor) handler;
				
				allHandlerMenuExecutor.add(handlerMenu);
			}
		}
		
		return allHandlerMenuExecutor;
	}
}
