package net.ardeus.runechat.handler;

import java.util.ArrayList;
import java.util.Collection;

import net.ardeus.runechat.RuneChat;

public abstract class HandlerMetrics extends Handler {
	
	protected HandlerMetrics(RuneChat plugin) {
		super(plugin);
	}
	
	public static Collection<HandlerMetrics> getAllHandlerMetrics() {
		final Collection<Handler> allHandler = getAllHandler();
		final Collection<HandlerMetrics> allHandlerMetrics = new ArrayList<HandlerMetrics>();
		
		for (Handler handler : allHandler) {
			if (handler instanceof HandlerMetrics) {
				final HandlerMetrics handlerMetrics = (HandlerMetrics) handler;
				
				allHandlerMetrics.add(handlerMetrics);
			}
		}
		
		return allHandlerMetrics;
	}
}
