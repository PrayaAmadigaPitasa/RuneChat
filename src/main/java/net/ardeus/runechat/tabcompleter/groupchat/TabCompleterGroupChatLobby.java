package net.ardeus.runechat.tabcompleter.groupchat;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.command.Commands;
import net.ardeus.runechat.tabcompleter.TabCompleterArgument;

public final class TabCompleterGroupChatLobby extends TabCompleterArgument {

	private static final Commands COMMAND = Commands.GROUPCHAT_LOBBY;
	
	private TabCompleterGroupChatLobby(Plugin plugin, String mainArgument) {
		super(plugin, mainArgument);
	}
	
	private static class TabCompleterHerbalismAboutSingleton {
		private static final TabCompleterGroupChatLobby instance;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			final String mainArgument = COMMAND.getMain();
			
			instance = new TabCompleterGroupChatLobby(plugin, mainArgument);
		}
	}
	
	protected static final TabCompleterGroupChatLobby getInstance() {
		return TabCompleterHerbalismAboutSingleton.instance;
	}

	@Override
	public List<String> execute(CommandSender sender, String[] args) {
		final List<String> tabList = new ArrayList<String>();
		
		if (args.length == 2) {
			tabList.add("");
		}
		
		return tabList;
	}
}
