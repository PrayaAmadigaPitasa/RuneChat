package net.ardeus.runechat.tabcompleter.groupchat;

import net.ardeus.runechat.tabcompleter.TabCompleterArgument;
import net.ardeus.runechat.tabcompleter.TabCompleterBaseTree;

public final class TabCompleterGroupChat extends TabCompleterBaseTree {

	private static final String COMMAND = "GroupChat";
	
	private TabCompleterGroupChat() {
		super(COMMAND);
		
		final TabCompleterArgument tabCompleterArgumentCreate = TabCompleterGroupChatCreate.getInstance();
		final TabCompleterArgument tabCompleterArgumentDelete = TabCompleterGroupChatDelete.getInstance();
		final TabCompleterArgument tabCompleterArgumentEnter = TabCompleterGroupChatEnter.getInstance();
		final TabCompleterArgument tabCompleterArgumentInvitation = TabCompleterGroupChatInvitation.getInstance();
		final TabCompleterArgument tabCompleterArgumentInvite = TabCompleterGroupChatInvite.getInstance();
		final TabCompleterArgument tabCompleterArgumentKick = TabCompleterGroupChatKick.getInstance();
		final TabCompleterArgument tabCompleterArgumentList = TabCompleterGroupChatList.getInstance();
		final TabCompleterArgument tabCompleterArgumentLobby = TabCompleterGroupChatLobby.getInstance();
		final TabCompleterArgument tabCompleterArgumentName = TabCompleterGroupChatName.getInstance();
		final TabCompleterArgument tabCompleterArgumentQuit = TabCompleterGroupChatQuit.getInstance();
		final TabCompleterArgument tabCompleterArgumentRole = TabCompleterGroupChatRole.getInstance();
		final TabCompleterArgument tabCompleterArgumentToggle = TabCompleterGroupChatToggle.getInstance();
		
		register(tabCompleterArgumentCreate);
		register(tabCompleterArgumentDelete);
		register(tabCompleterArgumentEnter);
		register(tabCompleterArgumentInvitation);
		register(tabCompleterArgumentInvite);
		register(tabCompleterArgumentKick);
		register(tabCompleterArgumentList);
		register(tabCompleterArgumentLobby);
		register(tabCompleterArgumentName);
		register(tabCompleterArgumentQuit);
		register(tabCompleterArgumentRole);
		register(tabCompleterArgumentToggle);
	}
	
	private static class CommandHerbalismSingleton {
		private static final TabCompleterGroupChat instance = new TabCompleterGroupChat();
	}
	
	public static final TabCompleterGroupChat getInstance() {
		return CommandHerbalismSingleton.instance;
	}
}