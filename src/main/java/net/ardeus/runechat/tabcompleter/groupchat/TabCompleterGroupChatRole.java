package net.ardeus.runechat.tabcompleter.groupchat;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.command.Commands;
import net.ardeus.runechat.groupchat.GroupChatPlayer;
import net.ardeus.runechat.manager.game.GameManager;
import net.ardeus.runechat.manager.game.GroupChatPlayerManager;
import net.ardeus.runechat.tabcompleter.TabCompleterArgument;

public final class TabCompleterGroupChatRole extends TabCompleterArgument {

	private static final Commands COMMAND = Commands.GROUPCHAT_ROLE;
	
	private TabCompleterGroupChatRole(Plugin plugin, String mainArgument) {
		super(plugin, mainArgument);
	}
	
	private static class TabCompleterHerbalismAboutSingleton {
		private static final TabCompleterGroupChatRole instance;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			final String mainArgument = COMMAND.getMain();
			
			instance = new TabCompleterGroupChatRole(plugin, mainArgument);
		}
	}
	
	protected static final TabCompleterGroupChatRole getInstance() {
		return TabCompleterHerbalismAboutSingleton.instance;
	}

	@Override
	public List<String> execute(CommandSender sender, String[] args) {
		final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
		final GameManager gameManager = plugin.getGameManager();
		final GroupChatPlayerManager groupChatPlayerManager = gameManager.getGroupChatPlayerManager();
		final List<String> tabList = new ArrayList<String>();
		
		if (sender instanceof Player) {
			final Player player = (Player) sender;
			final GroupChatPlayer groupChatPlayer = groupChatPlayerManager.getGroupChatPlayer(player);
			
			if (args.length == 4) {
				tabList.add("Admin");
				tabList.add("Member");
			} else if (args.length == 4) {
				final List<Integer> groupChatIds = groupChatPlayer.getGroupChatIds();
				
				if (groupChatIds.isEmpty()) {
					tabList.add("");
				} else {
					for (int groupChatId : groupChatIds) {
						tabList.add(String.valueOf(groupChatId));
					}
				}
			}
		}
		
		return tabList;
	}
}
