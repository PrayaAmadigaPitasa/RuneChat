package net.ardeus.runechat.tabcompleter;

import org.bukkit.command.TabCompleter;

public abstract class TabCompleterBase implements TabCompleter {
	
	private final String command;
	
	TabCompleterBase(String command) {
		if (command == null) {
			throw new IllegalArgumentException();
		} else {
			this.command = command;
		}
	}
	
	public final String getCommand() {
		return this.command;
	}
}