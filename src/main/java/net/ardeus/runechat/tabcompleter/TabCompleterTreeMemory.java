package net.ardeus.runechat.tabcompleter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.manager.game.TabCompleterBaseManager;
import net.ardeus.runechat.tabcompleter.groupchat.TabCompleterGroupChat;
import net.ardeus.runechat.tabcompleter.runechat.TabCompleterRuneChat;

public final class TabCompleterTreeMemory extends TabCompleterBaseManager {

	private final Map<String, TabCompleterBase> mapTabCompleterBase = new HashMap<String, TabCompleterBase>();
	
	private TabCompleterTreeMemory(RuneChat plugin) {
		super(plugin);
		
		final TabCompleterBaseTree tabCompleterRuneChat = TabCompleterRuneChat.getInstance();
		final TabCompleterBaseTree tabCompleterGroupChat = TabCompleterGroupChat.getInstance();
		
		register(tabCompleterRuneChat);
		register(tabCompleterGroupChat);
	}
	
	private static class CommandTreeMemorySingleton {
		private static final TabCompleterTreeMemory instance;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			
			instance = new TabCompleterTreeMemory(plugin);
		}
	}
	
	public static final TabCompleterTreeMemory getInstance() {
		return CommandTreeMemorySingleton.instance;
	}
	
	@Override
	public final List<String> getCommands() {
		return new ArrayList<String>(this.mapTabCompleterBase.keySet());
	}
	
	@Override
	public final List<TabCompleterBase> getAllTabCompleterBases() {
		return new ArrayList<TabCompleterBase>(this.mapTabCompleterBase.values());
	}
	
	@Override
	public final TabCompleterBase getTabCompleterBase(String command) {
		if (command != null) {
			for (String key : this.mapTabCompleterBase.keySet()) {
				if (key.equalsIgnoreCase(command)) {
					return this.mapTabCompleterBase.get(key);
				}
			}
		}
		
		return null;
	}
	
	protected final boolean register(TabCompleterBase tabCompleterBase) {
		if (tabCompleterBase != null && !this.mapTabCompleterBase.containsValue(tabCompleterBase)) {
			final String command = tabCompleterBase.getCommand();
			final PluginCommand pluginCommand = Bukkit.getPluginCommand(command);
			
			if (pluginCommand != null) {
			
				this.mapTabCompleterBase.put(command, tabCompleterBase);
				
				pluginCommand.setTabCompleter(tabCompleterBase);
				return true;
			}
		}
		
		return false;
	}
}
