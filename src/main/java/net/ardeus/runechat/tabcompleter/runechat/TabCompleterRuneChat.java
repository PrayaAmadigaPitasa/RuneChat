package net.ardeus.runechat.tabcompleter.runechat;

import net.ardeus.runechat.tabcompleter.TabCompleterArgument;
import net.ardeus.runechat.tabcompleter.TabCompleterBaseTree;

public final class TabCompleterRuneChat extends TabCompleterBaseTree {

	private static final String COMMAND = "RuneChat";
	
	private TabCompleterRuneChat() {
		super(COMMAND);
		
		final TabCompleterArgument tabCompleterArgumentAbout = TabCompleterRuneChatAbout.getInstance();
		final TabCompleterArgument tabCompleterArgumentHelp = TabCompleterRuneChatHelp.getInstance();
		final TabCompleterArgument tabCompleterArgumentReload = TabCompleterRuneChatReload.getInstance();
		final TabCompleterArgument tabCompleterArgumentType = TabCompleterRuneChatType.getInstance();
		
		register(tabCompleterArgumentAbout);
		register(tabCompleterArgumentHelp);
		register(tabCompleterArgumentReload);
		register(tabCompleterArgumentType);
	}
	
	private static class CommandHerbalismSingleton {
		private static final TabCompleterRuneChat instance = new TabCompleterRuneChat();
	}
	
	public static final TabCompleterRuneChat getInstance() {
		return CommandHerbalismSingleton.instance;
	}
}