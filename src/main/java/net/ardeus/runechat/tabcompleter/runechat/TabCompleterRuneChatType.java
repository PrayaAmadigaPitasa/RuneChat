package net.ardeus.runechat.tabcompleter.runechat;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.command.Commands;
import net.ardeus.runechat.manager.game.ChatTypeManager;
import net.ardeus.runechat.manager.game.GameManager;
import net.ardeus.runechat.tabcompleter.TabCompleterArgument;

public final class TabCompleterRuneChatType extends TabCompleterArgument {

	private static final Commands COMMAND = Commands.RUNECHAT_TYPE;
	
	private TabCompleterRuneChatType(Plugin plugin, String mainArgument) {
		super(plugin, mainArgument);
	}
	
	private static class TabCompleterHerbalismTypeSingleton {
		private static final TabCompleterRuneChatType instance;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			final String mainArgument = COMMAND.getMain();
			
			instance = new TabCompleterRuneChatType(plugin, mainArgument);
		}
	}
	
	protected static final TabCompleterRuneChatType getInstance() {
		return TabCompleterHerbalismTypeSingleton.instance;
	}

	@Override
	public List<String> execute(CommandSender sender, String[] args) {
		final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
		final GameManager gameManager = plugin.getGameManager();
		final ChatTypeManager chatTypeManager = gameManager.getChatTypeManager();
		final List<String> tabList = new ArrayList<String>();
		
		if (args.length == 2) {
			final List<String> chatTypeIds = chatTypeManager.getChatTypeIds();
			
			if (chatTypeIds.isEmpty()) {
				tabList.add("");
			} else {
				tabList.addAll(chatTypeIds);
			}
		}
		
		return tabList;
	}
}
