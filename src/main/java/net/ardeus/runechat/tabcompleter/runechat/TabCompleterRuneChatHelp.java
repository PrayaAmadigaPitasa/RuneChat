package net.ardeus.runechat.tabcompleter.runechat;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.command.Commands;
import net.ardeus.runechat.tabcompleter.TabCompleterArgument;

public final class TabCompleterRuneChatHelp extends TabCompleterArgument {

	private static final Commands COMMAND = Commands.RUNECHAT_HELP;
	
	private TabCompleterRuneChatHelp(Plugin plugin, String mainArgument) {
		super(plugin, mainArgument);
	}
	
	private static class TabCompleterHerbalismHelpSingleton {
		private static final TabCompleterRuneChatHelp instance;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			final String mainArgument = COMMAND.getMain();
			
			instance = new TabCompleterRuneChatHelp(plugin, mainArgument);
		}
	}
	
	protected static final TabCompleterRuneChatHelp getInstance() {
		return TabCompleterHerbalismHelpSingleton.instance;
	}

	@Override
	public List<String> execute(CommandSender sender, String[] args) {
		final List<String> tabList = new ArrayList<String>();
		
		if (args.length == 2) {
			tabList.add("[<page>]");
		}
		
		return tabList;
	}
}
