package net.ardeus.runechat.tabcompleter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TabCompleterUtil;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.command.CommandArgument;
import net.ardeus.runechat.command.CommandBase;
import net.ardeus.runechat.command.CommandBaseTree;
import net.ardeus.runechat.manager.game.CommandBaseManager;
import net.ardeus.runechat.manager.game.GameManager;

public class TabCompleterBaseTree extends TabCompleterBase {
	
	private final Map<String, TabCompleterArgument> mapTabCompleterArgument; 
	
	protected TabCompleterBaseTree(String command) {
		this(command, null);
	}
	
	protected TabCompleterBaseTree(String command, Map<String, TabCompleterArgument> mapTabCompleterArgument) {
		super(command);
		
		this.mapTabCompleterArgument = mapTabCompleterArgument != null ? mapTabCompleterArgument : new HashMap<String, TabCompleterArgument>();
	}
	
	public final Collection<String> getMainArguments() {
		return this.mapTabCompleterArgument.keySet();
	}
	
	public final Collection<TabCompleterArgument> getAllTabCompleterArgument() {
		return this.mapTabCompleterArgument.values();
	}
	
	public final TabCompleterArgument getTabCompleterArgument(String argument) {
		if (argument != null) {
			for (String key : getMainArguments()) {
				if (key.equalsIgnoreCase(argument)) {
					return this.mapTabCompleterArgument.get(key);
				}
			}
		}
		
		return null;
	}
	
	public final boolean isRegistered(String mainArgument) {
		return getTabCompleterArgument(mainArgument) != null;
	}
	
	public final boolean isRegistered(TabCompleterArgument tabCompleterArgument) {
		return tabCompleterArgument != null ? this.mapTabCompleterArgument.containsValue(tabCompleterArgument) : false;
	}
	
	public final boolean register(TabCompleterArgument tabCompleterArgument) {
		if (tabCompleterArgument != null && !this.mapTabCompleterArgument.containsValue(tabCompleterArgument)) {
			final String mainArgument = tabCompleterArgument.getMainArgument();
			
			this.mapTabCompleterArgument.put(mainArgument, tabCompleterArgument);
			
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
		final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
		final GameManager gameManager = plugin.getGameManager();
		final CommandBaseManager commandBaseManager = gameManager.getCommandTreeManager();
		final List<String> tabList = new ArrayList<String>();
		
		if (command != null) {
			final String name = command.getName();
			final CommandBase commandBase = commandBaseManager.getCommandBase(name);
			
			if (commandBase != null && commandBase instanceof CommandBaseTree) {
				final CommandBaseTree commandBaseTree = (CommandBaseTree) commandBase;
				
				if (args.length > 1) {
					final String argument = args[0];
					final CommandArgument commandArgument = commandBaseTree.getCommandArgument(argument);
					
					if (commandArgument != null) {
						final String mainArgument = commandArgument.getMainArgument();
						final TabCompleterArgument tabCompleterArgument = getTabCompleterArgument(mainArgument);
						
						if (tabCompleterArgument != null) {
							final List<String> listTabArgument = tabCompleterArgument.execute(sender, args);
							
							if (listTabArgument != null) {
								if (!listTabArgument.isEmpty()) {
									tabList.addAll(listTabArgument);
								} else {
									tabList.add("");
								}
							} else {
								return null;
							}
						} else {
							tabList.add("");
						}
					}
				} else {
					final Collection<CommandArgument> allCommandArgument = commandBaseTree.getAllCommandArgument();
					
					for (CommandArgument commandArgument : allCommandArgument) {
						final String permission = commandArgument.getPermission();
						
						if (SenderUtil.hasPermission(sender, permission)) {
							final String mainArgument = commandArgument.getMainArgument();
							
							tabList.add(mainArgument);
						}
					}
				}
			}
		}
		
		return TabCompleterUtil.returnList(tabList, args);
	}
}