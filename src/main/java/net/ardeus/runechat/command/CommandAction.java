package net.ardeus.runechat.command;

import org.bukkit.command.CommandSender;

public interface CommandAction {

	public String getCommand();
	public String getPermission();
	public String getDescription(CommandSender sender);
	public void execute(CommandSender sender, String[] args);
}