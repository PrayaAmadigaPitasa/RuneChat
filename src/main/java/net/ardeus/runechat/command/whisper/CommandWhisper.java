package net.ardeus.runechat.command.whisper;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.command.CommandBaseFlat;
import net.ardeus.runechat.language.Language;
import net.ardeus.runechat.manager.player.PlayerChatManager;
import net.ardeus.runechat.manager.player.PlayerManager;
import net.ardeus.runechat.player.PlayerChat;

public final class CommandWhisper extends CommandBaseFlat {

	private static final String COMMAND = "Whisper";
	private static final String PERMISSION = null;
	
	private CommandWhisper(RuneChat plugin) {
		super(COMMAND, PERMISSION);
		
	}
	
	private static class CommandDreamFishSingleton {
		private static final CommandWhisper INSTANCE;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			
			INSTANCE = new CommandWhisper(plugin);
		}
	}
	
	public static final CommandWhisper getInstance() {
		return CommandDreamFishSingleton.INSTANCE;
	}

	@Override
	public final String getDescription(CommandSender sender) {
		return Language.TOOLTIP_WHISPER.getText(sender);
	}
	
	@Override
	public final void execute(CommandSender sender, String[] args) {
		final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerChatManager playerChatManager = playerManager.getPlayerChatManager();
		
		if (args.length < 2) {
			final String tooltip = TextUtil.getJsonTooltip(Language.TOOLTIP_WHISPER.getText(sender));
			final MessageBuild message = Language.ARGUMENT_WHISPER.getMessage(sender);
			
			message.sendMessage(sender, "tooltip", tooltip);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else if (!(sender instanceof Player)) {
			final MessageBuild message = Language.CONSOLE_COMMAND_FORBIDEN.getMessage(sender);
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final Player player = (Player) sender;
			final String textTarget = args[0];
			final Player target = PlayerUtil.getOnlinePlayer(textTarget);
			
			if (target == null) {
				final MessageBuild message = Language.PLAYER_TARGET_OFFLINE.getMessage(sender);
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else if (target.equals(player)) {
				final MessageBuild message = Language.COMMAND_WHISPER_FAILED_SELF.getMessage(sender);
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else {
				final PlayerChat playerChat = playerChatManager.getPlayerChat(player);
				final String message = TextUtil.concatenate(args, 2);
				
				playerChat.sendMessage(target, message);
				return;
			}
		}
	}
}