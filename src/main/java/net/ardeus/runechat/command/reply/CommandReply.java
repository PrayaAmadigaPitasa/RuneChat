package net.ardeus.runechat.command.reply;

import java.util.UUID;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.command.CommandBaseFlat;
import net.ardeus.runechat.language.Language;
import net.ardeus.runechat.manager.player.PlayerChatManager;
import net.ardeus.runechat.manager.player.PlayerManager;
import net.ardeus.runechat.player.PlayerChat;

public final class CommandReply extends CommandBaseFlat {

	private static final String COMMAND = "Reply";
	private static final String PERMISSION = null;
	
	private CommandReply(RuneChat plugin) {
		super(COMMAND, PERMISSION);
		
	}
	
	private static class CommandReplySingleton {
		private static final CommandReply INSTANCE;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			
			INSTANCE = new CommandReply(plugin);
		}
	}
	
	public static final CommandReply getInstance() {
		return CommandReplySingleton.INSTANCE;
	}

	@Override
	public final String getDescription(CommandSender sender) {
		return Language.TOOLTIP_REPLY.getText(sender);
	}
	
	@Override
	public final void execute(CommandSender sender, String[] args) {
		final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
		final PlayerManager playerManager = plugin.getPlayerManager();
		final PlayerChatManager playerChatManager = playerManager.getPlayerChatManager();
		
		if (args.length < 1) {
			final String tooltip = TextUtil.getJsonTooltip(Language.TOOLTIP_WHISPER.getText(sender));
			final MessageBuild message = Language.ARGUMENT_WHISPER.getMessage(sender);
			
			message.sendMessage(sender, "tooltip", tooltip);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else if (!(sender instanceof Player)) {
			final MessageBuild message = Language.CONSOLE_COMMAND_FORBIDEN.getMessage(sender);
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final Player player = (Player) sender;
			final PlayerChat playerChat = playerChatManager.getPlayerChat(player);
			final UUID lastSenderId = playerChat.getLastSenderId();
			
			if (lastSenderId == null) {
				final MessageBuild message = Language.COMMAND_REPLY_FAILED_NO_SENDER.getMessage(sender);
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else {
				final Player target = PlayerUtil.getOnlinePlayer(lastSenderId);
				
				if (target == null) {
					final MessageBuild message = Language.PLAYER_TARGET_OFFLINE.getMessage(sender);
					
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				} else {
					final String message = TextUtil.concatenate(args, 1);
					
					playerChat.replyMessage(message);
					return;
				}
			}
		}
	}
}