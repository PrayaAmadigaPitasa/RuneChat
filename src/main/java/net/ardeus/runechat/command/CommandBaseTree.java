package net.ardeus.runechat.command;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import com.praya.agarthalib.utility.SenderUtil;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.runechat.command.CommandArgument;
import net.ardeus.runechat.language.Language;

public abstract class CommandBaseTree extends CommandBase {

	private final String defaultArgument;
	private final Map<String, CommandArgument> mapCommandArgument; 
	
	protected CommandBaseTree(String command) {
		this(command, null, null);
	}
	
	protected CommandBaseTree(String command, String defaultArgument) {
		this(command, defaultArgument, null);
	}
	
	protected CommandBaseTree(String command, String defaultArgument, Map<String, CommandArgument> mapCommandArgument) {
		super(command);
		
		this.defaultArgument = defaultArgument != null ? defaultArgument : "help";
		this.mapCommandArgument = mapCommandArgument != null ? mapCommandArgument : new HashMap<String, CommandArgument>();
	}
	
	public final String getDefaultArgument() {
		return this.defaultArgument;
	}
	
	public final Collection<String> getMainArguments() {
		return this.mapCommandArgument.keySet();
	}
	
	public final Collection<CommandArgument> getAllCommandArgument() {
		return this.mapCommandArgument.values();
	}
	
	public final CommandArgument getCommandArgument(String argument) {
		if (argument != null) {
			for (String key : getMainArguments()) {
				if (key.equalsIgnoreCase(argument)) {
					return this.mapCommandArgument.get(key);
				}
			}
			
			for (CommandArgument commandArgument : this.mapCommandArgument.values()) {
				final List<String> aliases = commandArgument.getAliases();
				
				for (String aliase : aliases) {
					if (aliase.equalsIgnoreCase(argument)) {
						return commandArgument;
					}
				}
			}
		}
		
		return null;
	}
	
	public final boolean isRegistered(String mainArgument) {
		return getCommandArgument(mainArgument) != null;
	}
	
	public final boolean isRegistered(CommandArgument commandArgument) {
		return commandArgument != null ? this.mapCommandArgument.containsValue(commandArgument) : false;
	}
	
	protected final boolean register(CommandArgument commandArgument) {
		if (commandArgument != null && !getAllCommandArgument().contains(commandArgument)) {
			final String mainArgument = commandArgument.getMainArgument();
			
			this.mapCommandArgument.put(mainArgument, commandArgument);
			
			return true;
		} else {
			return false;
		}
	}

	@Override
	public final boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		final String argument = args.length > 0 ? args[0] : getDefaultArgument();
		final CommandArgument commandArgument = getCommandArgument(argument);
		
		if (commandArgument != null) {
			final String permission = commandArgument.getPermission();
			
			if (!SenderUtil.hasPermission(sender, permission)) {
				final MessageBuild message = Language.PERMISSION_LACK.getMessage(sender);
				
				message.sendMessage(sender, "permission", permission);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return true;
			} else {
				commandArgument.execute(sender, args);
				return true;
			}
		} else {
			final MessageBuild message = Language.ARGUMENT_INVALID_COMMAND.getMessage(sender);
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return true;
		}
	}
}