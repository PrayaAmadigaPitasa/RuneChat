package net.ardeus.runechat.command.runechat;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.SenderUtil;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.RuneChatConfig;
import net.ardeus.runechat.chat.ChatComponentConfig;
import net.ardeus.runechat.chat.ChatTypeConfig;
import net.ardeus.runechat.command.CommandArgument;
import net.ardeus.runechat.command.CommandConfig;
import net.ardeus.runechat.command.Commands;
import net.ardeus.runechat.handler.Handler;
import net.ardeus.runechat.language.Language;
import net.ardeus.runechat.language.LanguageConfig;
import net.ardeus.runechat.placeholder.PlaceholderConfig;

public final class CommandRuneChatReload extends CommandArgument {

	private static final Commands COMMAND = Commands.RUNECHAT_RELOAD;
	
	protected CommandRuneChatReload(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_RUNECHAT_RELOAD.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
		final RuneChatConfig mainConfig = plugin.getMainConfig();
		
		if (!COMMAND.checkPermission(sender)) {
			final String permission = COMMAND.getPermission();
			final MessageBuild message = Language.PERMISSION_LACK.getMessage(sender);
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final MessageBuild message = Language.COMMAND_RUNECHAT_RELOAD_SUCCESS.getMessage(sender);
			final PlaceholderConfig placeholderConfig = Handler.getHandler(PlaceholderConfig.class);
			final LanguageConfig languageConfig = Handler.getHandler(LanguageConfig.class);
			final CommandConfig commandConfig = Handler.getHandler(CommandConfig.class);
			final ChatComponentConfig chatComponentConfig = Handler.getHandler(ChatComponentConfig.class);
			final ChatTypeConfig chatTypeConfig = Handler.getHandler(ChatTypeConfig.class);
			
			mainConfig.setup();
			placeholderConfig.setup();
			languageConfig.setup();
			commandConfig.setup();
			
			chatComponentConfig.setup();
			chatTypeConfig.setup();
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
			return;
		}
	}
}
