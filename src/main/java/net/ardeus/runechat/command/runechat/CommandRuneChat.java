package net.ardeus.runechat.command.runechat;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.command.CommandArgument;
import net.ardeus.runechat.command.CommandBaseTree;

public final class CommandRuneChat extends CommandBaseTree {

	private static final String COMMAND = "RuneChat";
	private static final String DEFAULT_ARGUMENT = "Help";
	
	private CommandRuneChat(RuneChat plugin) {
		super(COMMAND, DEFAULT_ARGUMENT);
		
		final CommandArgument commandArgumentAbout = new CommandRuneChatAbout(plugin);
		final CommandArgument commandArgumentHelp = new CommandRuneChatHelp(plugin);
		final CommandArgument commandArgumentReload = new CommandRuneChatReload(plugin);
		final CommandArgument commandArgumentType = new CommandRuneChatType(plugin);
		
		register(commandArgumentAbout);
		register(commandArgumentHelp);
		register(commandArgumentReload);
		register(commandArgumentType);
	}
	
	private static class CommandDreamFishSingleton {
		private static final CommandRuneChat INSTANCE;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			
			INSTANCE = new CommandRuneChat(plugin);
		}
	}
	
	public static final CommandRuneChat getInstance() {
		return CommandDreamFishSingleton.INSTANCE;
	}
}