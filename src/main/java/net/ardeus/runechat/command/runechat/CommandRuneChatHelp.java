package net.ardeus.runechat.command.runechat;

import java.util.HashMap;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.command.CommandArgument;
import net.ardeus.runechat.command.CommandAction;
import net.ardeus.runechat.command.Commands;
import net.ardeus.runechat.language.Language;
import net.ardeus.runechat.manager.game.CommandBaseManager;
import net.ardeus.runechat.manager.game.GameManager;

public final class CommandRuneChatHelp extends CommandArgument {

	private static final Commands COMMAND = Commands.RUNECHAT_HELP;
	
	protected CommandRuneChatHelp(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_RUNECHAT_HELP.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
		final GameManager gameManager = plugin.getGameManager();
		final CommandBaseManager commandBaseManager = gameManager.getCommandTreeManager();
		
		if (!COMMAND.checkPermission(sender)) {
			final String permission = COMMAND.getPermission();
			final MessageBuild message = Language.PERMISSION_LACK.getMessage(sender);
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {			
			final List<CommandAction> allCommandActions = commandBaseManager.getAllCommandActions();
			final int size = allCommandActions.size();
			final int limit = 6;
			final int maxPage = size % limit == 0 ? (size / limit) : (size / limit) + 1;
			
			int page = 1;
			
			if (args.length > 1) {
				final String textPage = args[1];
				
				if (MathUtil.isNumber(textPage)) {
					page = MathUtil.parseInteger(textPage);
					page = MathUtil.limitInteger(page, 1, maxPage);
				}
			}
			
			final MessageBuild messageHeader = Language.HELP_HEADER.getMessage(sender);			
			final MessageBuild messagePage = Language.HELP_PAGE.getMessage(sender);
			final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
			
			String previousTooltip = "||&6&l◀||ttp: {text_previous_page}||cmd: /{plugin} help {previous_page}||";
			String nextTooltip = "||&6&l▶||ttp: {text_next_page}||cmd: /{plugin} help {next_page}||";
			
			mapPlaceholder.put("plugin", plugin.getPluginName());
			mapPlaceholder.put("page", String.valueOf(page));
			mapPlaceholder.put("maxpage", String.valueOf(maxPage));
			mapPlaceholder.put("previous_page", String.valueOf(page-1));
			mapPlaceholder.put("next_page", String.valueOf(page+1));
			mapPlaceholder.put("text_previous_page", Language.HELP_PREVIOUS_PAGE.getText(sender));
			mapPlaceholder.put("text_next_page", Language.HELP_NEXT_PAGE.getText(sender));
			
			previousTooltip = TextUtil.placeholder(mapPlaceholder, previousTooltip);
			nextTooltip = TextUtil.placeholder(mapPlaceholder, nextTooltip);
			
			mapPlaceholder.put("previous", previousTooltip);
			mapPlaceholder.put("next", nextTooltip);
			
			messageHeader.sendMessage(sender, mapPlaceholder);
			SenderUtil.sendMessage(sender, "", true);
			messagePage.sendMessage(sender, mapPlaceholder);
			
			for (int index = ((page - 1) * limit); index < page * limit && index < size; index++) {
				final CommandAction commandAction = allCommandActions.get(index);
				final String command = commandAction.getCommand();
				final String tooltipDescription = TextUtil.getJsonTooltip(commandAction.getDescription(sender));
				final MessageBuild message = Language.ARGUMENT_FORMAT_USAGE.getMessage(sender);
				final String mainArgument;
				
				if (commandAction instanceof CommandArgument) {
					final CommandArgument commandArgument = (CommandArgument) commandAction;
					
					mainArgument = commandArgument.getMainArgument();
				} else {
					mainArgument = null;
				}
				
				mapPlaceholder.put("command", command);
				mapPlaceholder.put("tooltip", tooltipDescription);
				mapPlaceholder.put("main_argument", mainArgument);
				
				message.sendMessage(sender, mapPlaceholder);
			}
			
			messagePage.sendMessage(sender, mapPlaceholder);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);	
			return;
		}
	}
}
