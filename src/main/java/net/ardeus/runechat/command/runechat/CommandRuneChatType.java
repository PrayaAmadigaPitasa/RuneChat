package net.ardeus.runechat.command.runechat;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.chat.ChatType;
import net.ardeus.runechat.command.CommandArgument;
import net.ardeus.runechat.command.Commands;
import net.ardeus.runechat.language.Language;
import net.ardeus.runechat.manager.game.ChatTypeManager;
import net.ardeus.runechat.manager.game.GameManager;
import net.ardeus.runechat.manager.player.PlayerChatManager;
import net.ardeus.runechat.manager.player.PlayerManager;
import net.ardeus.runechat.player.PlayerChat;

public final class CommandRuneChatType extends CommandArgument {

	private static final Commands COMMAND = Commands.RUNECHAT_TYPE;
	
	protected CommandRuneChatType(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_RUNECHAT_RELOAD.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
		final GameManager gameManager = plugin.getGameManager();
		final PlayerManager playerManager = plugin.getPlayerManager();
		final ChatTypeManager chatTypeManager = gameManager.getChatTypeManager();
		final PlayerChatManager playerChatManager = playerManager.getPlayerChatManager();
		
		if (!COMMAND.checkPermission(sender)) {
			final String permission = COMMAND.getPermission();
			final MessageBuild message = Language.PERMISSION_LACK.getMessage(sender);
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else if (!(sender instanceof Player)) {
			final MessageBuild message = Language.CONSOLE_COMMAND_FORBIDEN.getMessage(sender);
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else if (args.length < 2) {
			final String tooltip = TextUtil.getJsonTooltip(Language.TOOLTIP_RUNECHAT_TYPE.getText(sender));
			final MessageBuild message = Language.ARGUMENT_RUNECHAT_TYPE.getMessage(sender);
			
			message.sendMessage(sender, "tooltip", tooltip);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final Player player = (Player) sender;
			final String textType = args[1];
			final PlayerChat playerChat = playerChatManager.getPlayerChat(player);
			final ChatType chatType = chatTypeManager.getChatType(textType);
			
			if (chatType == null) {
				final MessageBuild message = Language.ARGUMENT_INVALID_CHAT_TYPE.getMessage(sender);
				
				message.sendMessage(sender, "chat_type", textType);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else {
				final String chatTypeId = chatType.getId();
				final MessageBuild message = Language.COMMAND_RUNECHAT_TYPE_SUCCESS.getMessage(sender);
				
				playerChat.setChatType(chatType);
				message.sendMessage(sender, "chat_type", chatTypeId);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
				return;
			}
		}
	}
}
