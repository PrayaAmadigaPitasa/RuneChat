package net.ardeus.runechat.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.SenderUtil;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.language.Language;
import net.ardeus.runechat.manager.plugin.PlaceholderManager;
import net.ardeus.runechat.manager.plugin.PluginManager;

public abstract class CommandBaseFlat extends CommandBase implements CommandAction {
	
	private final String permission;
	
	protected CommandBaseFlat(String command) {
		this(command, null);
	}
	
	protected CommandBaseFlat(String command, String permission) {
		super(command);
		
		this.permission = permission;
	}
	
	@Override
	public final String getPermission() {
		return this.permission;
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
		final PluginManager pluginManager = plugin.getPluginManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final String description = placeholderManager.getPlaceholder("none");
		
		return description;
	}
	
	@Override
	public final boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		final String permission = getPermission();
		
		if (!SenderUtil.hasPermission(sender, permission)) {
			final MessageBuild message = Language.PERMISSION_LACK.getMessage(sender);
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return true;
		} else {
			execute(sender, args);
			return true;
		}
	}
}