package net.ardeus.runechat.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.manager.plugin.PlaceholderManager;
import net.ardeus.runechat.manager.plugin.PluginManager;

public abstract class CommandArgument implements CommandAction {

	private final Plugin plugin;
	private final String mainArgument;
	private final String permission;
	private final List<String> aliases;
	
	public CommandArgument(Plugin plugin, String mainArgument) {
		this(plugin, mainArgument, null);
	}
	
	public CommandArgument(Plugin plugin, String mainArgument, String permission) {
		this(plugin, mainArgument, permission, null);
	}
	
	public CommandArgument(Plugin plugin, String mainArgument, String permission, List<String> aliases) {
		if (plugin == null || mainArgument == null) {
			throw new IllegalArgumentException();
		} else {
			this.plugin = plugin;
			this.mainArgument = mainArgument;
			this.permission = permission;
			this.aliases = aliases != null ? aliases : new ArrayList<String>();
		}
	}
	
	public abstract void execute(CommandSender sender, String[] args);
	
	public final Plugin getPlugin() {
		return this.plugin;
	}
	
	public final String getMainArgument() {
		return this.mainArgument;
	}
	
	public final String getPermission() {
		return this.permission;
	}
	
	public final List<String> getAliases() {
		return this.aliases;
	}
	
	public final CommandBaseTree getCommandBaseTree() {
		final CommandBaseMemory commandBaseMemory = CommandBaseMemory.getInstance();
		
		for (CommandBase commandBase : commandBaseMemory.mapCommandBase.values()) {
			if (commandBase instanceof CommandBaseTree) {
				final CommandBaseTree commandBaseTree = (CommandBaseTree) commandBase;
				
				if (commandBaseTree.isRegistered(this)) {
					return commandBaseTree;
				}
			}
		}
		
		return null;
	}
	
	@Override
	public final String getCommand() {
		final CommandBaseTree commandBaseTree = getCommandBaseTree();
		
		return commandBaseTree != null ? commandBaseTree.getCommand() : null;
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
		final PluginManager pluginManager = plugin.getPluginManager();
		final PlaceholderManager placeholderManager = pluginManager.getPlaceholderManager();
		final String description = placeholderManager.getPlaceholder("none");
		
		return description;
	}
}