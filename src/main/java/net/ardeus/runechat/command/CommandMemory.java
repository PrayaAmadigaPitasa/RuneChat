package net.ardeus.runechat.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.plugin.java.JavaPlugin;

import core.praya.agarthalib.builder.command.CommandBuild;
import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.command.CommandConfig;
import net.ardeus.runechat.manager.plugin.CommandManager;

public final class CommandMemory extends CommandManager {

	private final CommandConfig commandConfig;
	
	private CommandMemory(RuneChat plugin) {
		super(plugin);
		
		this.commandConfig = new CommandConfig(plugin);
	};
	
	private static class CommandMemoryHelper {
		private static final CommandMemory instance;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			
			instance = new CommandMemory(plugin);
		}
	}
	
	public static final CommandMemory getInstance() {
		return CommandMemoryHelper.instance;
	}
	
	public final CommandConfig getCommandConfig() {
		return this.commandConfig;
	}
	
	@Override
	public final List<String> getCommandIds() {
		final Map<String, CommandBuild> mapCommandBuild = getCommandConfig().mapCommandBuild;
		
		return new ArrayList<String>(mapCommandBuild.keySet());
	}
	
	@Override
	public final List<CommandBuild> getAllCommandBuild() {
		final Map<String, CommandBuild> mapCommandBuild = getCommandConfig().mapCommandBuild;
		
		return new ArrayList<CommandBuild>(mapCommandBuild.values());
	}
	
	@Override
	public final CommandBuild getCommandBuild(String id) {
		if (id != null) {
			final Map<String, CommandBuild> mapCommandBuild = getCommandConfig().mapCommandBuild;
			
			for (String key : mapCommandBuild.keySet()) {
				if (key.equalsIgnoreCase(id)) {
					return getCommandConfig().mapCommandBuild.get(key);
				}
			}
		}
		
		return null;
	}
}
