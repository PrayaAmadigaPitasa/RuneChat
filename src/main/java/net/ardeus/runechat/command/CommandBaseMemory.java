package net.ardeus.runechat.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.command.PluginCommand;
import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.command.groupchat.CommandGroupChat;
import net.ardeus.runechat.command.reply.CommandReply;
import net.ardeus.runechat.command.runechat.CommandRuneChat;
import net.ardeus.runechat.command.whisper.CommandWhisper;
import net.ardeus.runechat.manager.game.CommandBaseManager;

public final class CommandBaseMemory extends CommandBaseManager {

	protected final Map<String, CommandBase> mapCommandBase = new HashMap<String, CommandBase>();
	
	private CommandBaseMemory(RuneChat plugin) {
		super(plugin);
		
		final CommandBase commandRuneChat = CommandRuneChat.getInstance();
		final CommandBase commandGroupChat = CommandGroupChat.getInstance();
		final CommandBase commandWhisper = CommandWhisper.getInstance();
		final CommandBase commandReply = CommandReply.getInstance();
		
		register(commandRuneChat);
		register(commandGroupChat);
		register(commandWhisper);
		register(commandReply);
	}
	
	private static class CommandTreeMemorySingleton {
		private static final CommandBaseMemory instance;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			
			instance = new CommandBaseMemory(plugin);
		}
	}
	
	public static final CommandBaseMemory getInstance() {
		return CommandTreeMemorySingleton.instance;
	}
	
	@Override
	public final List<String> getCommands() {
		return new ArrayList<String>(this.mapCommandBase.keySet());
	}
	
	@Override
	public final List<CommandBase> getAllCommandBases() {
		return new ArrayList<CommandBase>(this.mapCommandBase.values());
	}
	
	@Override
	public final CommandBase getCommandBase(String command) {
		if (command != null) {
			for (String key : this.mapCommandBase.keySet()) {
				if (key.equalsIgnoreCase(command)) {
					return this.mapCommandBase.get(key);
				}
			}
		}
		
		return null;
	}
	
	protected final boolean register(CommandBase commandBase) {
		if (commandBase != null && !this.mapCommandBase.containsValue(commandBase)) {
			final String command = commandBase.getCommand();
			final PluginCommand pluginCommand = Bukkit.getPluginCommand(command);
			
			if (pluginCommand != null) {
			
				this.mapCommandBase.put(command, commandBase);
				
				pluginCommand.setExecutor(commandBase);
				return true;
			}
		}
		
		return false;
	}
}
