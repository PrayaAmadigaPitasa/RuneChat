package net.ardeus.runechat.command.groupchat;

import java.util.HashMap;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.PlayerUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.command.CommandArgument;
import net.ardeus.runechat.command.Commands;
import net.ardeus.runechat.groupchat.GroupChat;
import net.ardeus.runechat.groupchat.GroupChatMember;
import net.ardeus.runechat.groupchat.GroupChatPlayer;
import net.ardeus.runechat.groupchat.GroupChatRole;
import net.ardeus.runechat.language.Language;
import net.ardeus.runechat.manager.game.GameManager;
import net.ardeus.runechat.manager.game.GroupChatManager;
import net.ardeus.runechat.manager.game.GroupChatPlayerManager;

public final class CommandGroupChatKick extends CommandArgument {

	private static final Commands COMMAND = Commands.GROUPCHAT_KICK;
	
	protected CommandGroupChatKick(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_GROUPCHAT_KICK.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
		final GameManager gameManager = plugin.getGameManager();
		final GroupChatManager groupChatManager = gameManager.getGroupChatManager();
		final GroupChatPlayerManager groupChatPlayerManager = gameManager.getGroupChatPlayerManager();
		
		if (!COMMAND.checkPermission(sender)) {
			final String permission = COMMAND.getPermission();
			final MessageBuild message = Language.PERMISSION_LACK.getMessage(sender);
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else if (!(sender instanceof Player)) {
			final MessageBuild message = Language.CONSOLE_COMMAND_FORBIDEN.getMessage(sender);
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else if (args.length < 2) {
			final String tooltip = TextUtil.getJsonTooltip(Language.TOOLTIP_GROUPCHAT_KICK.getText(sender));
			final MessageBuild message = Language.ARGUMENT_GROUPCHAT_KICK.getMessage(sender);
			
			message.sendMessage(sender, "tooltip", tooltip);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final String textTarget = args[1];
			final OfflinePlayer target = PlayerUtil.getPlayer(textTarget);
			
			if (target == null) {
				final MessageBuild message = Language.PLAYER_NOT_EXISTS.getMessage(sender);
				
				message.sendMessage(sender, "player", textTarget);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else {
				final Player player = (Player) sender;
				
				if (player.equals(target)) {
					final MessageBuild message = Language.COMMAND_GROUPCHAT_KICK_FAILED_SELF.getMessage(sender);
					
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				} else {
					final GroupChatPlayer groupChatPlayer = groupChatPlayerManager.getGroupChatPlayer(player);
					final GroupChatPlayer groupChatTarget = groupChatPlayerManager.getGroupChatPlayer(target);
					final GroupChat groupChat;
					
					if (args.length > 2) {
						final String textGroupChatId = args[2];
						
						if (!MathUtil.isNumber(textGroupChatId)) {
							final MessageBuild message = Language.ARGUMENT_INVALID_GROUP_ID.getMessage(sender);
							
							message.sendMessage(sender);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return;
						} else {
							final int groupChatId = MathUtil.parseInteger(textGroupChatId);
							final GroupChat groupChatInvite = groupChatManager.getGroupChat(groupChatId);
							
							if (groupChatInvite == null) {
								final MessageBuild message = Language.ARGUMENT_GROUP_NOT_EXISTS.getMessage(sender);
								
								message.sendMessage(sender, "groupchat_id", String.valueOf(groupChatId));
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return;
							} else if (!groupChatInvite.isMember(player)) {
								final MessageBuild message = Language.GROUP_CHAT_PLAYER_NOT_MEMBER.getMessage(sender);
								
								message.sendMessage(sender, "groupchat_name", groupChatInvite.getName());
								SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
								return;
							} else {
								groupChat = groupChatInvite;
							}
						}
					} else {
						final GroupChat groupChatActive = groupChatPlayer.getActiveGroupChat();
						
						if (groupChatActive == null) {
							final MessageBuild message = Language.GROUP_CHAT_PLAYER_ACTIVE_NOT_SET.getMessage(sender);
							
							message.sendMessage(sender);
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return;
						} else {
							groupChat = groupChatActive;
						}
					}
					
					final GroupChatMember groupChatMember = groupChatPlayer.getGroupChatMember(groupChat);
					final GroupChatRole groupChatRole = groupChatMember.getRole();
					final String groupChatName = groupChat.getName();
					final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
					
					mapPlaceholder.put("sender", sender.getName());
					mapPlaceholder.put("target", target.getName());
					mapPlaceholder.put("member", target.getName());
					mapPlaceholder.put("groupchat_name", groupChatName);
					mapPlaceholder.put("groupchat_id", String.valueOf(groupChat.getId()));
					
					if (!groupChatRole.equals(GroupChatRole.ADMIN)) {
						final MessageBuild message = Language.GROUP_CHAT_PLAYER_NOT_ADMIN.getMessage(sender);
						
						message.sendMessage(sender, mapPlaceholder);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return;
					} else if (!groupChatTarget.isJoinGroupChat(groupChat)) {
						final MessageBuild message = Language.ARGUMENT_MEMBER_NOT_EXISTS.getMessage(sender);
						
						message.sendMessage(sender, mapPlaceholder);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return;
					} else {
						if (target.isOnline()) {
							final Player playerTarget = (Player) target;
							final MessageBuild messageToTarget = Language.COMMAND_GROUPCHAT_KICK_SUCCESS_TO_TARGET.getMessage(playerTarget);
							
							messageToTarget.sendMessage(playerTarget, mapPlaceholder);
							SenderUtil.playSound(playerTarget, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_ON);
						}
						
						final MessageBuild messageToSender = Language.COMMAND_GROUPCHAT_KICK_SUCCESS_TO_SENDER.getMessage(sender);
						
						groupChatTarget.addGroupChatInvitation(groupChat);
						messageToSender.sendMessage(sender, mapPlaceholder);
						SenderUtil.playSound(sender, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_ON);
						return;
					}
				}
			}
		}
	}
}