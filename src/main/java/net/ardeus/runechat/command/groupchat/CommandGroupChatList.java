package net.ardeus.runechat.command.groupchat;

import java.util.HashMap;
import java.util.List;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.command.CommandArgument;
import net.ardeus.runechat.command.Commands;
import net.ardeus.runechat.groupchat.GroupChat;
import net.ardeus.runechat.groupchat.GroupChatPlayer;
import net.ardeus.runechat.language.Language;
import net.ardeus.runechat.manager.game.GameManager;
import net.ardeus.runechat.manager.game.GroupChatPlayerManager;

public final class CommandGroupChatList extends CommandArgument {

	private static final Commands COMMAND = Commands.GROUPCHAT_LIST;
	
	protected CommandGroupChatList(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_GROUPCHAT_LIST.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
		final GameManager gameManager = plugin.getGameManager();
		final GroupChatPlayerManager groupChatPlayerManager = gameManager.getGroupChatPlayerManager();
		
		if (!COMMAND.checkPermission(sender)) {
			final String permission = COMMAND.getPermission();
			final MessageBuild message = Language.PERMISSION_LACK.getMessage(sender);
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else if (!(sender instanceof Player)) {
			final MessageBuild message = Language.CONSOLE_COMMAND_FORBIDEN.getMessage(sender);
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final Player player = (Player) sender;
			final GroupChatPlayer groupChatPlayer = groupChatPlayerManager.getGroupChatPlayer(player);
			final List<GroupChat> groupChats = groupChatPlayer.getGroupChats();
			final int size = groupChats.size();
			
			if (size == 0) {
				final MessageBuild message = Language.GROUP_CHAT_PLAYER_EMPTY.getMessage(sender);
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else {
				final int limit = 6;
				final int maxPage = size % limit == 0 ? (size / limit) : (size / limit) + 1;
				
				int page = 1;
				
				if (args.length > 1) {
					final String textPage = args[1];
					
					if (MathUtil.isNumber(textPage)) {
						page = Math.min(maxPage, MathUtil.parseInteger(textPage));
					}
				}
				
				final MessageBuild messageHeader = Language.GROUP_CHAT_LIST_HEADER.getMessage(sender);			
				final MessageBuild messagePage = Language.GROUP_CHAT_LIST_PAGE.getMessage(sender);
				final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
				final HashMap<String, String> mapPlaceholderList = new HashMap<String, String>();
				
				String previousTooltip = "||&6&l◀||ttp: {text_previous_page}||cmd: /groupchat list {previous_page}||";
				String nextTooltip = "||&6&l▶||ttp: {text_next_page}||cmd: /groupchat list {next_page}||";
				
				mapPlaceholder.put("page", String.valueOf(page));
				mapPlaceholder.put("maxpage", String.valueOf(maxPage));
				mapPlaceholder.put("previous_page", String.valueOf(page-1));
				mapPlaceholder.put("next_page", String.valueOf(page+1));
				mapPlaceholder.put("text_previous_page", Language.HELP_PREVIOUS_PAGE.getText(sender));
				mapPlaceholder.put("text_next_page", Language.HELP_NEXT_PAGE.getText(sender));
				
				previousTooltip = TextUtil.placeholder(mapPlaceholder, previousTooltip);
				nextTooltip = TextUtil.placeholder(mapPlaceholder, nextTooltip);
				
				mapPlaceholder.put("previous", previousTooltip);
				mapPlaceholder.put("next", nextTooltip);
				
				messageHeader.sendMessage(sender, mapPlaceholder);
				SenderUtil.sendMessage(sender, "", true);
				messagePage.sendMessage(sender, mapPlaceholder);
				
				for (int index = ((page - 1) * limit); index < page * limit && index < size; index++) {
					final GroupChat groupChat = groupChats.get(index);
					final String groupChatName = groupChat.getName();
					final int groupChatId = groupChat.getId();
					final String tooltipKey = "&8&l[&2&l+&8&l]&r";
					final String tooltipDescription = Language.TOOLTIP_GROUPCHAT_LIST_DESCRIPTION.getText(sender); 
					final String tooltip = "||" + tooltipKey + "||ttp: " + tooltipDescription + "||cmd: /groupchat enter " + groupChatId + "||";
					final MessageBuild message = Language.ARGUMENT_FORMAT_GROUPCHAT_LIST.getMessage(sender);
					
					mapPlaceholderList.clear();
					mapPlaceholderList.put("groupchat", groupChatName);
					mapPlaceholderList.put("groupchat_name", groupChatName);
					mapPlaceholderList.put("groupchat_id", String.valueOf(groupChatId));
					mapPlaceholderList.put("tooltip", tooltip);
					
					message.sendMessage(sender, mapPlaceholderList);
				}
				
				messagePage.sendMessage(sender, mapPlaceholder);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);	
				return;
			}
		}
	}
}
