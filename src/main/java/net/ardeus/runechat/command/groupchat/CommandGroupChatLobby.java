package net.ardeus.runechat.command.groupchat;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.SenderUtil;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.command.CommandArgument;
import net.ardeus.runechat.command.Commands;
import net.ardeus.runechat.groupchat.GroupChat;
import net.ardeus.runechat.groupchat.GroupChatPlayer;
import net.ardeus.runechat.language.Language;
import net.ardeus.runechat.manager.game.GameManager;
import net.ardeus.runechat.manager.game.GroupChatPlayerManager;

public final class CommandGroupChatLobby extends CommandArgument {

	private static final Commands COMMAND = Commands.GROUPCHAT_LOBBY;
	
	protected CommandGroupChatLobby(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_GROUPCHAT_LOBBY.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
		final GameManager gameManager = plugin.getGameManager();
		final GroupChatPlayerManager groupChatPlayerManager = gameManager.getGroupChatPlayerManager();
		
		if (!COMMAND.checkPermission(sender)) {
			final String permission = COMMAND.getPermission();
			final MessageBuild message = Language.PERMISSION_LACK.getMessage(sender);
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else if (!(sender instanceof Player)) {
			final MessageBuild message = Language.CONSOLE_COMMAND_FORBIDEN.getMessage(sender);
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final Player player = (Player) sender;
			final GroupChatPlayer groupChatPlayer = groupChatPlayerManager.getGroupChatPlayer(player);
			final GroupChat groupChat = groupChatPlayer.getActiveGroupChat();
			
			if (groupChat == null) {
				final MessageBuild message = Language.GROUP_CHAT_PLAYER_ACTIVE_NOT_SET.getMessage(sender);
				
				message.sendMessage(sender);
				SenderUtil.playSound(sender, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_OFF);
				return;
			} else {
				final String groupChatName = groupChat.getName();
				final UUID groupChatUniqueId = groupChat.getUniqueId();
				final int groupChatId = groupChat.getId();
				final MessageBuild message = Language.COMMAND_GROUPCHAT_LOBBY_SUCCESS.getMessage(sender);
				final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
				
				mapPlaceholder.put("groupchat", groupChatName);
				mapPlaceholder.put("groupchat_name", groupChatName);
				mapPlaceholder.put("groupchat_uuid", groupChatUniqueId.toString());
				mapPlaceholder.put("groupchat_id", String.valueOf(groupChatId));
				
				groupChatPlayer.removeActiveGroupChat();
				message.sendMessage(sender, mapPlaceholder);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
				return;
			}
		}
	}
}