package net.ardeus.runechat.command.groupchat;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.RuneChatConfig;
import net.ardeus.runechat.command.CommandArgument;
import net.ardeus.runechat.command.Commands;
import net.ardeus.runechat.groupchat.GroupChat;
import net.ardeus.runechat.groupchat.GroupChatMember;
import net.ardeus.runechat.groupchat.GroupChatPlayer;
import net.ardeus.runechat.groupchat.GroupChatRole;
import net.ardeus.runechat.language.Language;
import net.ardeus.runechat.manager.game.GameManager;
import net.ardeus.runechat.manager.game.GroupChatManager;
import net.ardeus.runechat.manager.game.GroupChatPlayerManager;

public final class CommandGroupChatName extends CommandArgument {

	private static final Commands COMMAND = Commands.GROUPCHAT_NAME;
	
	protected CommandGroupChatName(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_GROUPCHAT_NAME.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
		final RuneChatConfig mainConfig = plugin.getMainConfig();
		final GameManager gameManager = plugin.getGameManager();
		final GroupChatManager groupChatManager = gameManager.getGroupChatManager();
		final GroupChatPlayerManager groupChatPlayerManager = gameManager.getGroupChatPlayerManager();
		
		if (!COMMAND.checkPermission(sender)) {
			final String permission = COMMAND.getPermission();
			final MessageBuild message = Language.PERMISSION_LACK.getMessage(sender);
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else if (args.length < 2) {
			final String tooltip = TextUtil.getJsonTooltip(Language.TOOLTIP_GROUPCHAT_NAME.getText(sender));
			final MessageBuild message = Language.ARGUMENT_GROUPCHAT_NAME.getMessage(sender);
			
			message.sendMessage(sender, "tooltip", tooltip);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final String groupChatName = args[1];
			final int limit = mainConfig.getChatGroupMaxLength();
			
			if (groupChatName.length() > limit) {
				final MessageBuild message = Language.GROUP_CHAT_NAME_LENGTH_LIMIT.getMessage(sender);
				
				message.sendMessage(sender, "limit", String.valueOf(limit));
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else {
				final GroupChat groupChat;
				
				if (args.length > 2) {
					final String textId = args[2];
					
					if (!MathUtil.isNumber(textId)) {
						final MessageBuild message = Language.ARGUMENT_INVALID_GROUP_ID.getMessage(sender);
						
						message.sendMessage(sender);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return;
					} else {
						final int id = MathUtil.parseInteger(textId);
						final GroupChat groupChatArgument = groupChatManager.getGroupChat(id);
						
						if (groupChatArgument == null) {
							final MessageBuild message = Language.ARGUMENT_GROUP_NOT_EXISTS.getMessage(sender);
							
							message.sendMessage(sender, "groupchat_id", String.valueOf(id));
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return;
						} else {
							groupChat = groupChatArgument;
						}
					}	
				} else {
					final Player player = (Player) sender;
					final GroupChatPlayer groupChatPlayer = groupChatPlayerManager.getGroupChatPlayer(player);
					final GroupChat activeGroupChat = groupChatPlayer.getActiveGroupChat();
					
					if (activeGroupChat == null) {
						final MessageBuild message = Language.GROUP_CHAT_PLAYER_ACTIVE_NOT_SET.getMessage(sender);
						
						message.sendMessage(sender);
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return;
					} else {
						groupChat = activeGroupChat;
					}
				}
				
				if (sender instanceof Player) {
					final Player player = (Player) sender;
					final GroupChatPlayer groupChatPlayer = groupChatPlayerManager.getGroupChatPlayer(player);
					final GroupChatMember groupChatMember = groupChatPlayer.getGroupChatMember(groupChat);
					
					if (groupChatMember == null) {
						final MessageBuild message = Language.GROUP_CHAT_PLAYER_NOT_MEMBER.getMessage(sender);
						
						message.sendMessage(sender, "groupchat_name", groupChat.getName());
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return;
					} else {
						final GroupChatRole groupChatRole = groupChatMember.getRole();
						
						if (!groupChatRole.equals(GroupChatRole.ADMIN)) {
							final MessageBuild message = Language.GROUP_CHAT_PLAYER_NOT_ADMIN.getMessage(sender);
							
							message.sendMessage(sender, "groupchat_name", groupChat.getName());
							SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
							return;
						}
					}
				}
				
				final UUID groupChatUniqueId = groupChat.getUniqueId();
				final int groupChatId = groupChat.getId();
				final MessageBuild message = Language.GROUP_CHAT_PLAYER_NOT_ADMIN.getMessage(sender);
				final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
				
				mapPlaceholder.put("groupchat", groupChatName);
				mapPlaceholder.put("groupchat_name", groupChatName);
				mapPlaceholder.put("groupchat_uuid", groupChatUniqueId.toString());
				mapPlaceholder.put("groupchat_id", String.valueOf(groupChatId));
				
				groupChat.setName(groupChatName);
				message.sendMessage(sender, mapPlaceholder);
				SenderUtil.playSound(sender, SoundEnum.ENTITY_EXPERIENCE_ORB_PICKUP);
				return;
			}
		}
	}
}