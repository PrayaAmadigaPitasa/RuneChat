package net.ardeus.runechat.command.groupchat;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.SenderUtil;
import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.command.CommandArgument;
import net.ardeus.runechat.command.Commands;
import net.ardeus.runechat.groupchat.GroupChatPlayer;
import net.ardeus.runechat.language.Language;
import net.ardeus.runechat.manager.game.GameManager;
import net.ardeus.runechat.manager.game.GroupChatPlayerManager;

public final class CommandGroupChatToggle extends CommandArgument {

	private static final Commands COMMAND = Commands.GROUPCHAT_TOGGLE;
	
	protected CommandGroupChatToggle(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_GROUPCHAT_TOGGLE.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
		final GameManager gameManager = plugin.getGameManager();
		final GroupChatPlayerManager groupChatPlayerManager = gameManager.getGroupChatPlayerManager();
		
		if (!COMMAND.checkPermission(sender)) {
			final String permission = COMMAND.getPermission();
			final MessageBuild message = Language.PERMISSION_LACK.getMessage(sender);
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else if (!(sender instanceof Player)) {
			final MessageBuild message = Language.CONSOLE_COMMAND_FORBIDEN.getMessage(sender);
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final Player player = (Player) sender;
			final GroupChatPlayer groupChatPlayer = groupChatPlayerManager.getGroupChatPlayer(player);
			final boolean toggle;
			
			if (args.length > 1) {
				final String textFlag = args[1];
				
				switch (textFlag.toUpperCase()) {
				case "YES":
				case "TRUE":
					toggle = true; break;
				case "NO":
				case "FALSE":
					toggle = false; break;
				default:
					toggle = !groupChatPlayer.getToggle(); 
				}
			} else {
				toggle = !groupChatPlayer.getToggle();
			}
			
			final MessageBuild message = (toggle ? Language.COMMAND_GROUPCHAT_TOGGLE_SUCCESS_ACTIVATED : Language.COMMAND_GROUPCHAT_TOGGLE_SUCCESS_DEACTIVATED).getMessage(sender);
			
			groupChatPlayer.setToggle(toggle);
			message.sendMessage(sender, "toggle", String.valueOf(toggle));
			SenderUtil.playSound(sender, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_ON);
			return;
		}
	}
}
