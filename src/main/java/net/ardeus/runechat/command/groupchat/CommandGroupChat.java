package net.ardeus.runechat.command.groupchat;

import org.bukkit.plugin.java.JavaPlugin;

import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.command.CommandArgument;
import net.ardeus.runechat.command.CommandBaseTree;

public final class CommandGroupChat extends CommandBaseTree {

	private static final String COMMAND = "GroupChat";
	private static final String DEFAULT_ARGUMENT = "Toggle";
	
	private CommandGroupChat(RuneChat plugin) {
		super(COMMAND, DEFAULT_ARGUMENT);
		
		final CommandArgument commandArgumentCreate = new CommandGroupChatCreate(plugin);
		final CommandArgument commandArgumentDelete = new CommandGroupChatDelete(plugin);
		final CommandArgument commandArgumentEnter = new CommandGroupChatEnter(plugin);
		final CommandArgument commandArgumentInvitation = new CommandGroupChatInvitation(plugin);
		final CommandArgument commandArgumentInvite = new CommandGroupChatInvite(plugin);
		final CommandArgument commandArgumentKick = new CommandGroupChatKick(plugin);
		final CommandArgument commandArgumentList = new CommandGroupChatList(plugin);
		final CommandArgument commandArgumentLobby = new CommandGroupChatLobby(plugin);
		final CommandArgument commandArgumentName = new CommandGroupChatName(plugin);
		final CommandArgument commandArgumentQuit = new CommandGroupChatQuit(plugin);
		final CommandArgument commandArgumentRole = new CommandGroupChatRole(plugin);
		final CommandArgument commandArgumentToggle = new CommandGroupChatToggle(plugin);
		
		register(commandArgumentCreate);
		register(commandArgumentDelete);
		register(commandArgumentEnter);
		register(commandArgumentInvitation);
		register(commandArgumentInvite);
		register(commandArgumentKick);
		register(commandArgumentList);
		register(commandArgumentLobby);
		register(commandArgumentName);
		register(commandArgumentQuit);
		register(commandArgumentRole);
		register(commandArgumentToggle);
	}
	
	private static class CommandDreamFishSingleton {
		private static final CommandGroupChat INSTANCE;
		
		static {
			final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
			
			INSTANCE = new CommandGroupChat(plugin);
		}
	}
	
	public static final CommandGroupChat getInstance() {
		return CommandDreamFishSingleton.INSTANCE;
	}
}