package net.ardeus.runechat.command.groupchat;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.RuneChatConfig;
import net.ardeus.runechat.command.CommandArgument;
import net.ardeus.runechat.command.Commands;
import net.ardeus.runechat.groupchat.GroupChat;
import net.ardeus.runechat.language.Language;
import net.ardeus.runechat.manager.game.GameManager;
import net.ardeus.runechat.manager.game.GroupChatManager;

public final class CommandGroupChatCreate extends CommandArgument {

	private static final Commands COMMAND = Commands.GROUPCHAT_CREATE;
	
	protected CommandGroupChatCreate(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_GROUPCHAT_CREATE.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
		final RuneChatConfig mainConfig = plugin.getMainConfig();
		final GameManager gameManager = plugin.getGameManager();
		final GroupChatManager groupChatManager = gameManager.getGroupChatManager();
		
		if (!COMMAND.checkPermission(sender)) {
			final String permission = COMMAND.getPermission();
			final MessageBuild message = Language.PERMISSION_LACK.getMessage(sender);
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else if (!(sender instanceof Player)) {
			final MessageBuild message = Language.CONSOLE_COMMAND_FORBIDEN.getMessage(sender);
			
			message.sendMessage(sender);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else if (args.length < 2) {
			final String tooltip = TextUtil.getJsonTooltip(Language.TOOLTIP_GROUPCHAT_CREATE.getText(sender));
			final MessageBuild message = Language.ARGUMENT_GROUPCHAT_CREATE.getMessage(sender);
			
			message.sendMessage(sender, "tooltip", tooltip);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final String textGroupChat = args[1];
			final int limit = mainConfig.getChatGroupMaxLength();
			
			if (textGroupChat.length() > limit) {
				final MessageBuild message = Language.GROUP_CHAT_NAME_LENGTH_LIMIT.getMessage(sender);
				
				message.sendMessage(sender, "limit", String.valueOf(limit));
				SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
				return;
			} else {
				final Player player = (Player) sender;
				final GroupChat groupChat = groupChatManager.createGroupChat(textGroupChat, player);
				
				if (groupChat == null) {
					final MessageBuild message = Language.COMMAND_GROUPCHAT_CREATE_FAILED.getMessage(sender);
					
					message.sendMessage(sender, "groupchat", textGroupChat);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				} else {
					final String groupChatName = groupChat.getName();
					final UUID groupChatUniqueId = groupChat.getUniqueId();
					final int groupChatId = groupChat.getId();
					final MessageBuild message = Language.COMMAND_GROUPCHAT_CREATE_SUCCESS.getMessage(sender);
					final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
					
					mapPlaceholder.put("groupchat", groupChatName);
					mapPlaceholder.put("groupchat_name", groupChatName);
					mapPlaceholder.put("groupchat_uuid", groupChatUniqueId.toString());
					mapPlaceholder.put("groupchat_id", String.valueOf(groupChatId));
					
					message.sendMessage(sender, mapPlaceholder);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_PLAYER_LEVELUP);
					return;
				}
			}	
		}
	}
}
