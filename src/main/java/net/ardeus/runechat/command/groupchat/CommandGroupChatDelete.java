package net.ardeus.runechat.command.groupchat;

import java.util.HashMap;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import com.praya.agarthalib.utility.MathUtil;
import com.praya.agarthalib.utility.SenderUtil;
import com.praya.agarthalib.utility.TextUtil;

import core.praya.agarthalib.builder.message.MessageBuild;
import core.praya.agarthalib.enums.branch.SoundEnum;
import net.ardeus.runechat.RuneChat;
import net.ardeus.runechat.command.CommandArgument;
import net.ardeus.runechat.command.Commands;
import net.ardeus.runechat.groupchat.GroupChat;
import net.ardeus.runechat.groupchat.GroupChatMember;
import net.ardeus.runechat.groupchat.GroupChatPlayer;
import net.ardeus.runechat.groupchat.GroupChatRole;
import net.ardeus.runechat.language.Language;
import net.ardeus.runechat.manager.game.GameManager;
import net.ardeus.runechat.manager.game.GroupChatManager;
import net.ardeus.runechat.manager.game.GroupChatPlayerManager;

public final class CommandGroupChatDelete extends CommandArgument {

	private static final Commands COMMAND = Commands.GROUPCHAT_DELETE;
	
	protected CommandGroupChatDelete(Plugin plugin) {
		super(plugin, COMMAND.getMain(), COMMAND.getPermission(), COMMAND.getAliases());
	}
	
	@Override
	public String getDescription(CommandSender sender) {
		return Language.TOOLTIP_GROUPCHAT_DELETE.getText();
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		final RuneChat plugin = JavaPlugin.getPlugin(RuneChat.class);
		final GameManager gameManager = plugin.getGameManager();
		final GroupChatManager groupChatManager = gameManager.getGroupChatManager();
		final GroupChatPlayerManager groupChatPlayerManager = gameManager.getGroupChatPlayerManager();
		
		if (!COMMAND.checkPermission(sender)) {
			final String permission = COMMAND.getPermission();
			final MessageBuild message = Language.PERMISSION_LACK.getMessage(sender);
			
			message.sendMessage(sender, "permission", permission);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else if (args.length < (sender instanceof Player ? 1 : 2)) {
			final String tooltip = TextUtil.getJsonTooltip(Language.TOOLTIP_GROUPCHAT_DELETE.getText(sender));
			final MessageBuild message = Language.ARGUMENT_GROUPCHAT_DELETE.getMessage(sender);
			
			message.sendMessage(sender, "tooltip", tooltip);
			SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
			return;
		} else {
			final GroupChat groupChat;
			
			if (args.length > 1) {
				final String textId = args[1];
				
				if (!MathUtil.isNumber(textId)) {
					final MessageBuild message = Language.ARGUMENT_INVALID_GROUP_ID.getMessage(sender);
					
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				} else {
					final int id = MathUtil.parseInteger(textId);
					final GroupChat groupChatArgument = groupChatManager.getGroupChat(id);
					
					if (groupChatArgument == null) {
						final MessageBuild message = Language.ARGUMENT_GROUP_NOT_EXISTS.getMessage(sender);
						
						message.sendMessage(sender, "groupchat_id", String.valueOf(id));
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return;
					} else {
						groupChat = groupChatArgument;
					}
				}	
			} else {
				final Player player = (Player) sender;
				final GroupChatPlayer groupChatPlayer = groupChatPlayerManager.getGroupChatPlayer(player);
				final GroupChat activeGroupChat = groupChatPlayer.getActiveGroupChat();
				
				if (activeGroupChat == null) {
					final MessageBuild message = Language.GROUP_CHAT_PLAYER_ACTIVE_NOT_SET.getMessage(sender);
					
					message.sendMessage(sender);
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				} else {
					groupChat = activeGroupChat;
				}
			}
			
			if (sender instanceof Player) {
				final Player player = (Player) sender;
				final GroupChatPlayer groupChatPlayer = groupChatPlayerManager.getGroupChatPlayer(player);
				final GroupChatMember groupChatMember = groupChatPlayer.getGroupChatMember(groupChat);
				
				if (groupChatMember == null) {
					final MessageBuild message = Language.GROUP_CHAT_PLAYER_NOT_MEMBER.getMessage(sender);
					
					message.sendMessage(sender, "groupchat_name", groupChat.getName());
					SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
					return;
				} else {
					final GroupChatRole groupChatRole = groupChatMember.getRole();
					
					if (!groupChatRole.equals(GroupChatRole.ADMIN)) {
						final MessageBuild message = Language.GROUP_CHAT_PLAYER_NOT_ADMIN.getMessage(sender);
						
						message.sendMessage(sender, "groupchat_name", groupChat.getName());
						SenderUtil.playSound(sender, SoundEnum.ENTITY_BLAZE_DEATH);
						return;
					}
				}
			}
			
			final String groupChatName = groupChat.getName();
			final int groupChatId = groupChat.getId();
			final MessageBuild message = Language.COMMAND_GROUPCHAT_DELETE_SUCCESS.getMessage(sender);
			final HashMap<String, String> mapPlaceholder = new HashMap<String, String>();
			
			mapPlaceholder.put("groupchat", groupChatName);
			mapPlaceholder.put("groupchat_name", groupChatName);
			mapPlaceholder.put("groupchat_id", String.valueOf(groupChatId));
			
			groupChat.disband();
			message.sendMessage(sender, mapPlaceholder);
			SenderUtil.playSound(sender, SoundEnum.BLOCK_WOOD_BUTTON_CLICK_ON);
			return;
		}
	}
}