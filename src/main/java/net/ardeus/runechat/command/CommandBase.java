package net.ardeus.runechat.command;

import org.bukkit.command.CommandExecutor;

public abstract class CommandBase implements CommandExecutor {

	private final String command;
	
	CommandBase(String command) {
		if (command == null) {
			throw new IllegalArgumentException();
		} else {
			this.command = command;
		}
	}
	
	public final String getCommand() {
		return this.command;
	}
}